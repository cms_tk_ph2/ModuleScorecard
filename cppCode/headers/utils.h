#ifndef utils_h__
#define utils_h__

#include <string>

// helpful functions related to naming conventions
std::string getDateTimeString();

#endif