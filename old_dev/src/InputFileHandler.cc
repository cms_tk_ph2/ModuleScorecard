#include "headers/InputFileHandler.h"
#include "headers/Result.h"
#include <sstream>
#include <iostream>
#include <fstream>


#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>
//************************************************************************
InputFileHandler::InputFileHandler(){

}

//************************************************************************
void InputFileHandler::openFile (std::string fileName)
{
    fResultFile = TFile::Open(fileName.c_str());
}

//************************************************************************
bool InputFileHandler::getFile (TFile* &file)
{

    if (fResultFile){
        file = fResultFile; 
        return true;
    } 
    else {
        return false;
    }

}
//************************************************************************
bool InputFileHandler::setTopLevel(std::string topLevelName)
{
    if ( topLevelName.empty() ) return false;
    else {
        fTopLevelName = topLevelName;
        return true;
    }
    
}

// *******************************************************************************
std::string InputFileHandler::getModuleName(TDirectory *directory, std::string txtStart)
{
    std::string directoryPath = directory->GetPath();
    std::string modName = "";
    
    // C++ string parsing nonsense
    char *token = strtok(&directoryPath[0], "/");
    while (token != NULL)
    {
        
        std::string dirName = token; 
        if ( dirName.find(txtStart) != std::string::npos) {
            //std::cout << token << std::endl;
            modName = dirName.erase(0,txtStart.size());
            //std::cout << "Module ID " << modName <<  std::endl;
        }
        token = strtok(NULL, "/");

    }

    return modName;
}
// *******************************************************************************
std::string InputFileHandler::getIterationName(TDirectory *directory, std::string txtStart)
{
    std::string directoryPath = directory->GetPath();
    std::string iterName = "";    
    
    // C++ string parsing nonsense
    char *token = strtok(&directoryPath[0], "/");
    while (token != NULL)
    {
        
        std::string dirName = token; 
        if ( dirName.find(txtStart) != std::string::npos) {
            //std::cout << token << std::endl;
            iterName = dirName.erase(0,txtStart.size());
            //std::cout << "Iteration " << iterName <<  std::endl;
        }
        token = strtok(NULL, "/");

    }
    return iterName;
}


//************************************************************************
// Recursive function to loop through directories 
void InputFileHandler::getNextDirectory(TDirectory* currentDirectory)
{

    TIter keys( currentDirectory->GetListOfKeys() );
    TKey *dkey;
    while ( dkey = (TKey*)keys() ) {
        TObject *dobj = dkey->ReadObj();
        
        
        if ( dobj->IsA()->InheritsFrom("TDirectory") ){
            
            TDirectory *nextDir = (TDirectory*)dobj;
            
            // If directory name contains string of interest, append to results for analysis
            std::string name = dobj->GetName();
            if ( name.find(fTopLevelName) != std::string::npos ){
                Result *result = new Result;
                result->directory = nextDir;
                result->moduleName = getModuleName(nextDir, "OpticalGroup_");
                result->iterationName = getIterationName(nextDir, "Iteration_");
                fResultsToAnalyze.push_back(result);
            } 
            
            InputFileHandler::getNextDirectory(nextDir);
        }

    }

}


//************************************************************************
bool InputFileHandler::getResults(std::vector<Result*> &resultsToAnalyze)
{
// Recursively loop through file and identify which directories to analyze
//     file/Detector/Board/OpticalGroup/Hybrid/Chip/Channel
//     Identify modules with OpticalGroup_
//     Identify iterations

    if (fResultFile){
        
        InputFileHandler::getNextDirectory(gDirectory);

        if (fResultsToAnalyze.size() == 0) {
            std::cout << "No results found to analyze" << std::endl;
            return false;
        }
        else {
            
            for (Result *result : fResultsToAnalyze){
                std::cout << result->directory->GetPath() << std::endl; // full directory path
                std::cout << "Module " << result->moduleName << std::endl; // can tell you which module
                std::cout << "Iteration " << result->iterationName << std::endl; // can tell you which iteration

            }

            resultsToAnalyze = fResultsToAnalyze;
    
            return true;
        }

    } 
    else {
        return false;
    }


}

//************************************************************************
void InputFileHandler::closeFile(void)
{

}
