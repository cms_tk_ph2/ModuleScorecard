#ifndef Result_h__
#define Result_h__

#include <string>
#include <vector>
#include <map>
#include <iostream>

class DatabaseTable
{
public:
    DatabaseTable(void){}
    DatabaseTable(std::string tableName) : fTableName(tableName){}
    DatabaseTable(std::string tableName, std::vector<std::string> columnNames) : fTableName(tableName)
    {
        for(auto& column: columnNames)
            fValues[column] = "";
    }
    ~DatabaseTable(){}
    void addColumns(std::vector<std::string> columnNames)
    {
        for(auto& column: columnNames)
        {
            if(fValues.find(column) != fValues.end())
                std::cout << __PRETTY_FUNCTION__ << "ERROR: The column " << column << " in DatabaseTable " << fTableName << " already exist! Your code is junk! Fix it!" << std::endl;
            fValues[column] = "";
        }
    }
    void setValue(std::string column, std::string value)
    {
        fValues[column] = value;
    }
private:
    std::string fTableName = "";
    std::map<std::string, std::string> fValues;
};

class Component
{
public:
    Component(std::string name, std::string type, std::string parentFileDir) : fName(name), fType(type), fComponentDir(parentFileDir + "/" + name){}
    Component(){}
    ~Component(){}

    Component& addComponent(std::string name, std::string type, std::string parentFileDir)
    {
        return fSubComponents[name] = Component(name,type, fComponentDir);
    }
    DatabaseTable& addTable(std::string name)
    {
        return fTables[name];
    }
    DatabaseTable& setupTable(std::string name, std::vector<std::string> columnNames)
    {
        if(fTables.find(name) != fTables.end())
        {
            fTables[name].addColumns(columnNames);
        }
        return (fTables[name] = DatabaseTable(name, columnNames));
    }
    void clear(void){fTables.clear(); fSubComponents.clear();}
    ///////////////////////////////////////////////////////////////////////
    const std::string& getName(void) const {return fName;}

    ///////////////////////////////////////////////////////////////////////
    const std::string& getType(void) const {return fType;}

    ///////////////////////////////////////////////////////////////////////
    const std::string& getComponentDir(void) const {return fComponentDir;}

    ///////////////////////////////////////////////////////////////////////
    const std::map<std::string, Component>& getComponents() const
    {
        return fSubComponents;
    }
    ///////////////////////////////////////////////////////////////////////
    Component& getComponent(std::string name)
    {
        if(fSubComponents.find(name) != fSubComponents.end())
            return fSubComponents.find(name)->second;
        else
        {
            std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no component named: " << name << ". Your code is junk! Fix it!" << std::endl;
            return fSubComponents.begin()->second;//Returning the first element if the component is not found!
        }
    }
    ///////////////////////////////////////////////////////////////////////
    const std::map<std::string, DatabaseTable>& getTables() const
    {
        return fTables;
    }
    ///////////////////////////////////////////////////////////////////////
    DatabaseTable& getTable(std::string name)
    {
        if(fTables.find(name) != fTables.end())
            return fTables.find(name)->second;
        else
        {
            std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no table named: " << name << ". Your code is junk! Fix it!" << std::endl;
            return fTables.begin()->second;//Returning the first element if the table is not found!
        }
    }
    void setProperties(std::string name, std::string type, std::string parentFileDir){fName = name; fType = type; fComponentDir = parentFileDir + "/" + name;}
private:
    std::string                          fName;
    std::string                          fType;
    std::string                          fComponentDir;
    std::map<std::string, Component>     fSubComponents;
    std::map<std::string, DatabaseTable> fTables;
};

class Result
{

public:
    Result();
    ~Result();

    ///////////////////////////////////////////////////
    //Methods
    void clear();
    void addHybrid   (std::string componentName);
    void addChip     (Component& hybrid, std::string componentName);
    void addTable    (Component& component, std::string tableName);
    ///////////////////////////////////////////////////
    //Getters
    Component& getOpticalGroup(void);
    Component& getHybrid(std::string name){return fOpticalGroup.getComponent(name);}
    const std::map<std::string, Component>& getHybrids(void) const;
    const std::map<std::string, Component>& getChips  (Component& hybrid) const;
    ///////////////////////////////////////////////////
    //Setters
    void setTableValue(Component& component, std::string tableName, std::string column, std::string value);


private:
    Component fOpticalGroup;
};

#endif

