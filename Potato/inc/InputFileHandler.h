#ifndef InputFileHandler_h__
#define InputFileHandler_h__

#include <string>
#include <vector>

class TFile;
class TH1;
class TDirectory;
class Result;
class Component;

// Manages the files with the results
// Handles complexity of different file structures
class InputFileHandler
{
public:
    InputFileHandler();
    void openFile (std::string fileName, std::string directory="");
    void closeFile(void);
    void makeResultTree(Result& result);

    ///////////////////////////////////////////////////
    //Getters
    TFile*               getFile   (void);
    TH1*                 getHisto  (std::string dirName, std::string name);
    std::vector<Result*> getResults(void);

private:
    ///////////////////////////////////////////////////
    //Methods
    void        getNextDirectory(TDirectory* currentDirectory, Component* component = nullptr);
    std::string getModuleName   (TDirectory* directory, std::string txtStart);
    std::string getIterationName(TDirectory* directory, std::string txtStart);

    ///////////////////////////////////////////////////
    //Variables
    TFile*               fResultFile       = nullptr;
    const std::string    fBoardName        = "Board"; // string to find top level for analysis
    const std::string    fOpticalGroupName = "OpticalGroup"; // string to find top level for analysis
    const std::string    fHybridName       = "Hybrid"; // string to find top level for analysis
    const std::string    fChipName         = "Chip"; // string to find top level for analysis
    std::string          fModuleFilesDir;
    std::string          fCurrentBoardName = "";
};

#endif
