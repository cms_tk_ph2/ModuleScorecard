# Data analysis tool for module evaluation aka POTATO

In development -- a GUI to be used for module evaluation/scoring during OT production

To run application
```
python gui/launch.py
```

## Resources

### POTATO
Potato depends on the `cmsdbldr` database utility package that needs to be pulled as submodule with the following commands:

```
git submodule init
git submodule update
```

The relevant part of the `resthub` database utility package (https://github.com/valdasraps/resthub.git) has been now included in the Potato repository (in the `gitlab_resthub` directory) so that the changes that were implemented to it could be easily accessed by all users. For sake of bookeeping the cahnges are also listed below. 
######

#### `resthub` changes
- commented out the compiler check in clients/cpp/include/json.hpp
```
//#elif defined(__GNUC__)
//    #define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
//    #if GCC_VERSION < 40900
//        #error "unsupported GCC version - see https://github.com/nlohmann/json#supported-compilers"
//    #endif
```


- added in clients/cpp/src/Request.cpp (after line 48) 
```
#if you are connecting to the database outside CERN
curl_easy_setopt(m_curl, CURLOPT_SSL_VERIFYPEER, false);
```

- changed in  clients/cpp/src/Query.cpp
every time ` "query/"+m_id` appears, it is replaced by `m_id`

### Install and run Potato (in progress)

```
git clone https://gitlab.cern.ch/cms_tk_ph2/ModuleScorecard.git
```
Add the submodules as explained also above:
```
git submodule init
git submodule update
```

Then

```
cd ModuleScorecard/Potato/
source setupPotato.sh
```
To create the make file run `qmake`, then comile with `make -f MakefileAll` . The next time you change the code you can use `make` to compile.

To run the code:
```
./Potato
```

#####
### GUI
https://realpython.com/qt-designer-python/

### To run 
Setup python virtual environment
```
python3 -m venv ./venv
source venv/bin/activate
```
In the virtual environment, install necessary packages
```
pip install --upgrade pip
pip3 install pyqt5 pyqt5-tools
pip3 install dict2xml 

source setup.sh # set's up root
```

Launch GUI designer
```
designer
```
### Analysis code


### Database
construction database twiki: https://twiki.cern.ch/twiki/bin/viewauth/CMS/ConstructionDB#Authorization_for_secure_access
