import Result

class Analyzer:

    def __init__(self):
        self.fResult = Result.Result()

    def getMetaData(self,metadata):
        print("This is a 2S module")

    def getResult(self):
        return self.fResult

    # Contains functions common to 2S and PS modules

    # Extract useful info about noise given the part name and hist of noise / channel
    def getPartNoise(self, inputHists, threshold=5):
        # threshold = number of sigma away from mean that qualifies as "outlier"
        # Strip is "bad" if its noise > is threshold x std of noise                                                        

        binContents = []

        for inputHist in inputHists:
            noiseHist = inputHist.Clone()
            binContents += [ noiseHist.GetBinContent(bin) for bin in range(1,noiseHist.GetNbinsX()) ]

        mean = sum(binContents)/len(binContents)
        std  = (sum( [ ( (binContent-mean)**2) for binContent in binContents] ) / len(binContents) ) **0.5

        disconnectedStrips = []
        badStrips = []
        badStripsDiff = []
        for i,binContent in enumerate(binContents):
            if binContent < 1:
                disconnectedStrips.append(i)
            if abs(binContent - mean) > threshold * std: # disconnected is a subset of bad                                                                  
                badStrips.append(i)
                badStripsDiff.append(binContent-mean)

        nDisconnectedStrips = len(disconnectedStrips)
        nBadStrips = len(badStrips)

        disconnectedStripsString = ""
        for strip in disconnectedStrips:
            disconnectedStripsString += str(strip)+","

        badStripsString = ""
        for strip in badStrips:
            badStripsString += str(strip)+","

        returnDict = {}
        returnDict["MeanNoise"] = mean
        returnDict["StdNoise"] = std
        returnDict["DisconnStrips"] = disconnectedStripsString
        returnDict["nDisconnStrips"] = nDisconnectedStrips
        returnDict["BadStrips"] = badStripsString
        returnDict["nBadStrips"] = nBadStrips
        
        # calculate outliers                                                                                                                              
        outliersHigh = []
        outliersLow = []
        for i,binContent in enumerate(binContents):
            if binContent > mean + threshold * std:
                outliersHigh.append(i)
            if binContent < mean - threshold * std:
                outliersLow.append(i)

        outliersLowString = ""
        for strip in outliersLow:
            outliersLowString += str(strip)+","
        returnDict["nOutliersLowNoise"] = len(outliersLow)
        returnDict["OutliersLowNoise"] = outliersLowString

        outliersHighString = ""
        for strip in outliersHigh:
            outliersHighString += str(strip)+","
        returnDict["nOutliersHighNoise"] = len(outliersHigh)
        returnDict["OutliersHighNoise"] = outliersHighString

        return returnDict
        
    # Extract useful info about pedestal given the part name and hist
    def getPartPedestal(self, inputHists, threshold=5):

        binContents = []

        for inputHist in inputHists:
            pedestalHist = inputHist.Clone()
            binContents += [ pedestalHist.GetBinContent(bin) for bin in range(1,pedestalHist.GetNbinsX()) ]

        mean = sum(binContents)/len(binContents)
        std  = (sum( [ ( (binContent-mean)**2) for binContent in binContents] ) / len(binContents) ) **0.5

        returnDict = {}
        returnDict["MeanPedestal"] = mean
        returnDict["StdPedestal"] = std

        # calculate outliers                                                                                                                                   
        outliersHigh = []
        outliersLow = []
        for i,binContent in enumerate(binContents):
            if binContent > mean + threshold * std:
                outliersHigh.append(i)
            if binContent < mean - threshold * std:
                outliersLow.append(i)

        outliersLowString = ""
        for strip in outliersLow:
            outliersLowString += str(strip)+","
        returnDict["nOutliersLowPedestal"] = len(outliersLow)
        returnDict["OutliersLowPedestal"] = outliersLowString

        outliersHighString = ""
        for strip in outliersHigh:
            outliersHighString += str(strip)+","
        returnDict["nOutliersHighPedestal"] = len(outliersHigh)
        returnDict["OutliersHighPedestal"] = outliersHighString

        return returnDict

    # Extract useful info about offset given the part name and hist
    def getPartOffset(self, inputHists, threshold=5):

        binContents = []

        for inputHist in inputHists:
            offsetHist = inputHist.Clone()
            binContents += [ offsetHist.GetBinContent(bin) for bin in range(1,offsetHist.GetNbinsX()) ]

        mean = sum(binContents)/len(binContents)
        std  = (sum( [ ( (binContent-mean)**2) for binContent in binContents] ) / len(binContents) ) **0.5

        returnDict = {}
        returnDict["MeanOffset"] = mean
        returnDict["StdOffset"] = std

        # calculate outliers                                                                                                                           
        outliersHigh = []
        outliersLow = []
        for i,binContent in enumerate(binContents):
            if binContent > mean + threshold * std:
                outliersHigh.append(i)
            if binContent < mean - threshold * std:
                outliersLow.append(i)

        outliersLowString = ""
        for strip in outliersLow:
            outliersLowString += str(strip)+","
        returnDict["nOutliersLowOffset"] = len(outliersLow)
        returnDict["OutliersLowOffset"] = outliersLowString

        outliersHighString = ""
        for strip in outliersHigh:
            outliersHighString += str(strip)+","
        returnDict["nOutliersHighOffset"] = len(outliersHigh)
        returnDict["OutliersHighOffset"] = outliersHighString

        return returnDict

    # Extract useful info about occupancy given the part name and hist                                                                                    
    def getPartOccupancy(self, inputHists, threshold=5):

        binContents = []

        for inputHist in inputHists:
            occupancyHist = inputHist.Clone()
            binContents += [ occupancyHist.GetBinContent(bin) for bin in range(1,occupancyHist.GetNbinsX()) ]

        mean = sum(binContents)/len(binContents)
        std  = (sum( [ ( (binContent-mean)**2) for binContent in binContents] ) / len(binContents) ) **0.5

        returnDict = {}
        returnDict["MeanOccupancy"] = mean
        returnDict["StdOccupancy"] = std

        # calculate outliers                                                                                                                                   
        outliersHigh = []
        outliersLow = []
        for i,binContent in enumerate(binContents):
            if binContent > mean + threshold * std:
                outliersHigh.append(i)
            if binContent < mean - threshold * std:
                outliersLow.append(i)

        outliersLowString = ""
        for strip in outliersLow:
            outliersLowString += str(strip)+","
        returnDict["nOutliersLowOccupancy"] = len(outliersLow)
        returnDict["OutliersLowOccupancy"] = outliersLowString

        outliersHighString = ""
        for strip in outliersHigh:
            outliersHighString += str(strip)+","
        returnDict["nOutliersHighOccupancy"] = len(outliersHigh)
        returnDict["OutliersHighOccupancy"] = outliersHighString

        return returnDict

    # Extract useful info about the IV scan
    def getIVScan(self, inputObject):
        # do something
        # edit the result
        #self.fResult["breakdown"] = function on inputObject
        return

#        result["moduleName"] = metadata.ModuleName
# part
# name
# timestamp of test
# iteration
