#include "headers/utils.h"

//************************************************************************
std::string getDateTimeString(){
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer,sizeof(buffer),"%d%m%Y_%H%M",timeinfo);
  std::string str(buffer);

  return str;
}
