#===============   Personalized configuration on this computer   ==============================================
if [ ${HOSTNAME} == "lenny01.fnal.gov" ]; then

  #===== Location of the ROOT components
  export ROOTSYS=/opt/local/root_v6.24.00
  export ROOTINC=${ROOTSYS}/include
  export ROOTLIB=${ROOTSYS}/lib

  #===== Location of the Qt components
  #export QTDIR=/opt/local/qt/5.12.4/gcc_64
  export QTCREATORDIR=/opt/local/qt/Tools/QtCreator
  #export QTLIB=${QTDIR}/lib 
  #export QTINC=${QTDIR}/include 
    
fi

#===== Final PATH definitions
#export PATH=${ROOTSYS}/bin:${QTDIR}/bin:${QTCREATORDIR}/bin:${PATH}
export PATH=${ROOTSYS}/bin:${QTCREATORDIR}/bin:${PATH}
