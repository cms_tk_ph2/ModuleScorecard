#include "headers/ResultHandler.h"
#include "headers/Result.h"

#include <sstream>
#include <iostream>

#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>
#include <TH1.h>
#include <TGraphErrors.h>

//************************************************************************
ResultHandler::ResultHandler(Result* &result){
    fResult = result;
}

//************************************************************************
void ResultHandler::doAnalysis()
{

    std::vector<std::string> hybrids = {"Hybrid_0","Hybrid_1"};
    std::vector<std::string> sides = {"Even","Odd"};
    
    std::string histName;
    std::string componentName;

    for( int h = 0; h < hybrids.size() ; h++){

        histName = "HybridStripNoiseDistribution";
        componentName = hybrids[h];
        noiseResult( histName, componentName, hybrids[h] ); 

        for( int s = 0; s < sides.size(); s++){

            histName = "HybridStripNoise" + sides[s] + "Distribution";
            componentName = hybrids[h] + "_Side_" + sides[s];
            noiseResult(histName, componentName, hybrids[h]);

        }
    }
}


// *******************************************************************************
//loop through subdirectories to find histogram with name inside of certain component
bool ResultHandler::grabHisto(TH1F* &histo, std::string histName, std::string component, TDirectory *d)
{
    
    TIter keys( d->GetListOfKeys() );
    TKey *dkey;
    while ( dkey = (TKey*)keys() ) {
        TObject *dobj = dkey->ReadObj();
        
        //if it's a directory, keep iterating        
        if ( dobj->IsA()->InheritsFrom( TDirectory::Class() ) ) {
            d->cd(dobj->GetName());
            TDirectory* subDir = gDirectory;
            subDir->cd();
            grabHisto(histo, histName, component, subDir);
        }
        //otherwise if it's a histogram that comes the directory corresponding to the compnent we want, return it
        if( dobj->IsA()->InheritsFrom( TH1::Class() ) && d->GetName() == component ) {
            std::string name = dobj->GetName();
            if ( name.find(histName) != std::string::npos) {
                std::cout << "Found histo " << dobj->GetName()  << std::endl;
                histo = (TH1F*)dobj;
                std::cout << "Histo integral: " << histo->Integral() << std::endl;
                histo->SetDirectory(0);
                return true;
            }
        }
    }
    return false;

}

//************************************************************************
void ResultHandler::noiseResult(std::string histName, std::string componentName, std::string component){

    // Get histogram for analysis
    TH1F* histo = new TH1F();  // clone?

    grabHisto(histo, histName, component, fResult->directory);

    TH1F* tmp = (TH1F*)histo->Clone();
    
    // Get mean and RMS noise
    TGraphErrors *g = new TGraphErrors(tmp); 
    float mean = g->GetMean(2) ;
    float rms = g->GetRMS(2);

    // Get disconnected strips or outliers
    int nDisconnected = 0;
    int nOutliers = 0;
    std::vector<int> disconnectedStrips = {};
    std::vector<int> outlierStrips = {};

    for (int bin=1; bin < tmp->GetNbinsX(); bin++)
    {
        //std::cout << bin << " " << tmp->GetBinContent(bin) << std::endl;
        int channel = bin-1;

        float val = tmp->GetBinContent(bin);
        if (val < 0.1 ) // don't hard code
        {
            nDisconnected += 1; 
            disconnectedStrips.push_back( channel );
        }
        if (abs(val-mean) > 3*rms) // don't hard code
        {
            nOutliers +=1; 
            outlierStrips.push_back( channel );
        }
    }

    addMeasurement( componentName+"_meanNoise"    , mean            );
    addMeasurement( componentName+"_RMSNoise"     , rms             );
    addMeasurement( componentName+"_nDisconnected", nDisconnected   );
    addMeasurement( componentName+"_nOutlier"     , nOutliers       );


}
//************************************************************************
void ResultHandler::addMeasurement(std::string label, float value){
    
    std::map<std::string, float>::iterator it = fResult->measurements.find(label);
    
    if( it == fResult->measurements.end() )
        fResult->measurements.insert( std::pair<std::string, float>(label, value));
    else
        it->second = value;

}
//************************************************************************
void ResultHandler::printResults(){

    std::cout << std::endl<< "PRINTING STORED RESULTS " << std::endl;
    std::cout << "module ID " << fResult->moduleName << std::endl;
    std::cout << "iteration " << fResult->iterationName << std::endl;

    for( auto it = fResult->measurements.begin(); it!= fResult->measurements.end(); it++){
        std::cout << it->first << " " << it->second << std::endl;
    }
}

//************************************************************************
void ResultHandler::updateResult(Result* &result)
{
    result = fResult;
}