#ifndef ROOT_GRAPHER_H
#define ROOT_GRAPHER_H

#include <QMainWindow>
#include <QStandardItemModel>
#include "TObject.h"
#include "root_canvas.h"
#include "qrootfile.h"

namespace Ui {
class root_grapher;
}

class root_grapher : public QMainWindow
{
    Q_OBJECT

public:
    explicit root_grapher(QWidget *parent = 0);
    ~root_grapher();

private slots:
    void plot_data(TObject *pGraph);
    void on_change_file_clicked();
    void updateCanvas();
    void on_histo_list_clicked(QModelIndex mIndex);

private:
    Ui::root_grapher *ui;
    TObject *fPlot;
    QROOTFile * fRootFile;
};

#endif // ROOT_GRAPHER_H
