all_files = {
    "0T_barrel" : [
    "Results_0T/Trimming/FEH_2S_Skeleton_Run35",
#    "Results_0T/Trimming/FEH_2S_Skeleton_Run38",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run43",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run45",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run49",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run50",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run51",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run52",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run56",
    "Results_0T/triggerscan/FEH_2S_Skeleton_Run57",
    ],

    "3T_barrel" : [
    "Results_3T/Trimming/FEH_2S_Skeleton_Run60",
    "Results_3T/Trimming/FEH_2S_Skeleton_Run61",
    "Results_3T/Trimming/FEH_2S_Skeleton_Run64",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run65",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run67",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run68",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run69",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run70",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run73",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run74",
    "Results_3T/triggerscan/FEH_2S_Skeleton_Run75",
    ],

    "1.5T_barrel" : [
    "Results_1p5T/Trimming/FEH_2S_Skeleton_Run79",
    "Results_1p5T/Trimming/FEH_2S_Skeleton_Run80",
    "Results_1p5T/Trimming/FEH_2S_Skeleton_Run81",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run82",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run83",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run86",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run88",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run89",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run90",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run92",
    "Results_1p5T/triggerscan/FEH_2S_Skeleton_Run93",
    ],

    "0T_endcap" : [
    "ResultsEC_0T/Trimming/FEH_2S_Skeleton_Run102",
    "ResultsEC_0T/Trimming/FEH_2S_Skeleton_Run105",
    "ResultsEC_0T/Trimming/FEH_2S_Skeleton_Run106",
    "ResultsEC_0T/FEH_2S_Skeleton_Run112",
    ],

    "3T_endcap" : [
    "ResultsEC_3T/Trimming/FEH_2S_Skeleton_Run128",
    "ResultsEC_3T/Trimming/FEH_2S_Skeleton_Run130",
    "ResultsEC_3T/Trimming/FEH_2S_Skeleton_Run131",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run134",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run135",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run136",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run137",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run138",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run139",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run140",
    "ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run141",
    ],

    "1.5T_endcap" : [
    "ResultsEC_1p5T/Trimming/FEH_2S_Skeleton_Run152",
    "ResultsEC_1p5T/Trimming/FEH_2S_Skeleton_Run158",
    "ResultsEC_1p5T/Trimming/FEH_2S_Skeleton_Run159",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run160",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run161",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run164",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run165",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run167",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run168",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run170",
    "ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run171",
    ]
}

