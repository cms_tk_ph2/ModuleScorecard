import Scorer 

class Scorer2S(Scorer.Scorer):

    def scoreModule(self, inputXML, metadata):
        metadataOut = self.getMetaData(metadata)
        print("Scoring a 2S module")

        # configurable thresholds
        from scoringThresholds import thresholds 
        
        # xml reader
        import xml.etree.ElementTree as ET
        tree = ET.parse(inputXML)
        root = tree.getroot()

        # Clean up and move stuff into functions
        moduleScore = {}

        # Score each hybrid
        for hybrid in root:

            if "Hybrid" not in hybrid.tag: 
                continue
                
            hybridScore = {}

            noiseScore = 0
            meanNoise = float(hybrid.find("MeanNoise").text)
            if   meanNoise < thresholds["noiseGood"] : noiseScore += 10 
            elif meanNoise < thresholds["noiseFair"] : noiseScore += 5
            hybridScore["noiseScore"] = noiseScore

            stripScore = 0 
            nBadStrips = float(hybrid.find("nBadStrips").text)
            if   nBadStrips < thresholds["hybridNBadStripsGood"] : stripScore += 10
            elif nBadStrips < thresholds["hybridNBadStripsFair"] : stripScore += 5

            hybridScore["stripScore"] = stripScore
            hybridScore["totalScore"] = noiseScore+stripScore

            #print(hybridScore)
            moduleScore[hybrid.tag] = hybridScore

            # Score each chip 
            for chip in hybrid:
                if "Chip" not in chip.tag : continue

                chipScore = {}

                noiseScore=0
                meanNoise = float(chip.find("MeanNoise").text)
                if   meanNoise < thresholds["noiseGood"] : noiseScore += 10 
                elif meanNoise < thresholds["noiseFair"] : noiseScore += 5
                chipScore["noiseScore"] = noiseScore

                stripScore = 0 
                nBadStrips = float(chip.find("nBadStrips").text)
                if   nBadStrips < thresholds["chipNBadStripsGood"] : stripScore += 10
                elif nBadStrips < thresholds["chipNBadStripsFair"] : stripScore += 5

                chipScore["stripScore"] = stripScore
                chipScore["totalScore"] = noiseScore+stripScore

                #print(chipScore)
                moduleScore[hybrid.tag,chip.tag] = chipScore
        

        # Calculate final module score...  some algebra based on scores of hybrids & chips
                
        print(moduleScore)
        print(self.fScore.fScoreDict)
        return self.fScore

