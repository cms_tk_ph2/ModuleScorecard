import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import * 
from PyQt5.QtCore import * 
import datetime

from launch_window import Ui_MainWindow as Ui_MainWindow_main

from analyze import *
from score import *

class MainW (QMainWindow, Ui_MainWindow_main):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
    
    def closeEvent(self, event):
      dialog = QMessageBox.information(self, 'Done?', 'Are you sure you want to close?', buttons = QMessageBox.Yes|QMessageBox.No)
      if dialog == QMessageBox.Yes:
        sys.exit()
      else:
        event.ignore()
    
    # parse input file to get username & moduleID from metadata tree
    def _getMetadata(self, inputFile):
      username = "lesya"
      moduleID = "100"
      return username, moduleID

    def launch_analyze_window(self):
      username, moduleID = self._getMetadata(self.InputFile)  
      self.analyze_window = AnalyzeModuleW(username, moduleID)
      self.analyze_window.show() 
    
    def launch_score_window(self):
      username, moduleID = self._getMetadata(self.InputFile)  
      self.score_window = ScoreModuleW(username, moduleID)
      self.score_window.show() 
    

if __name__ == '__main__':
    app = QApplication(sys.argv)
    myapp = MainW()
    myapp.show()
    sys.exit(app.exec_())
