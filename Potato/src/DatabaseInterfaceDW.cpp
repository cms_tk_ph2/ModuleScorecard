/****************************************************************************
** Potato: the Phase2 OuterTracker Analyzer of Test Outputs
**
** Copyright (C) 2021
**
** Authors:
**
** Lorenzo Uplegger   (FNAL)
** Fabio Ravera       (FNAL)
** Lesya Horyn        (FNAL)
** Jennet Dickinson   (FNAL)
** Karry DiPetrillo   (FNAL)
** Irene Zoi          (FNAL)
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include "DatabaseInterfaceDW.h"
#include "ui_DatabaseInterfaceDW.h"
#include <PyHelper.h>
#include <iostream>
#include "gitlab_resthub/clients/cpp/include/resthub.h"
using namespace resthub;

//===========================================================================
DatabaseInterfaceDW::DatabaseInterfaceDW(QMainWindow *parent) :
    BaseDW("Database Interface", parent),
    ui(new Ui::DatabaseInterfaceDW)
{
    ui->setupUi(this);
}

//===========================================================================
DatabaseInterfaceDW::~DatabaseInterfaceDW()
{
    delete ui;
}

//===========================================================================
void DatabaseInterfaceDW::on_databaseInterfaceUploadPB_clicked()
{
    std::cout << "Uploading file" << std::endl;
    QString potatoDir = qgetenv("POTATODIR");
    QString loaderDir = QString("sys.path.append(\"") + potatoDir + QString("/cmsdbldr/src/main/python/") + QString("\")");
    CPyInstance hInstance;
    PyRun_SimpleString("import sys");
    PyRun_SimpleString(loaderDir.toStdString().c_str());

    CPyObject pName = PyUnicode_FromString("cmsdbldr_client");
    CPyObject pModule = PyImport_Import(pName);
    if (!pModule)
        std::cout << "ERROR: Module not imported" << std::endl;

    CPyObject instance = PyObject_CallMethod(pModule, "LoaderClient", "");
    if (!instance)
        std::cout << "ERROR: instance not found" << std::endl;
    //cmd = ['-u', self.uploadUrl, '--login', pPathToFile]
    CPyObject kwargs = Py_BuildValue("[s:s:s:s]", "-u", "https://cmsdca.cern.ch/trk_loader/trker/int2r", "--login", "./temp/module_iv.xml");
    CPyObject result = PyObject_CallMethod(instance, "run", "[s:s:s:s]", "-u", "https://cmsdca.cern.ch/trk_loader/trker/int2r", "--login", "./temp/module_iv.xml");
    if (!result)
        std::cout << "ERROR: method not found" << std::endl;
}

//===========================================================================
void DatabaseInterfaceDW::on_databaseInterfaceGetRunNumberPB_clicked()
{
//    std::cout << "Getting run number" << std::endl;
//    Response response = runQuery("select r.run_number from trker_int2r.trk_ot_test_nextrun_v r");
//    std::cout << "Run number: " << response.str() << std::endl;

    std::string base_url    = "https://cmsomsdet.cern.ch/tracker-resthub/query";
    std::string upload_url  = "https://cmsdca.cern.ch/trk_loader/trker/int2r";
  //  Resthub resthub("http://dbloader-tracker.cern.ch:8113");

  //  Resthub resthub(upload_url);
    Resthub resthub(base_url);

    Response r;

  //  r = resthub.info();

  #define PRINT(method_inv) \
    std::cout << "resthub."#method_inv" response:\n";

  #define RUN_EX(method_inv) \
    std::cout << #method_inv" response:\n"; \
    std::cout << method_inv.str() << std::endl;

  #define RUN(method_inv) \
    std::cout << "resthub."#method_inv" response:\n"; \
    std::cout << resthub.method_inv.str() << std::endl;

  //  RUN(info());
  //  return EXIT_SUCCESS;

  //  RUN(folders());

  //  RUN(tables("int2r"));

  //  RUN(table("gem_int2r", "c10000000000000799"));

   // RUN(query("select r.run_number from trker_int2r.trk_ot_test_nextrun_v r"));

    Query q = resthub.query("select r.run_number from trker_int2r.trk_ot_test_nextrun_v r");
    RUN_EX(q.function("count"));
    RUN_EX(q.cache_delete());
    RUN_EX(q.cache());
  //  RUN_EX(q.csv({}, 1, 1000));
  //  RUN_EX(q.xml({}, 1, 1000));
    RUN_EX(q.json2({}, 1, 1000));


}

//===========================================================================
Response DatabaseInterfaceDW::runQuery(std::string query)
{
//#define RUN_EX(method_inv) \
//    std::cout << #method_inv" response:\n"; \
//    std::cout << method_inv.str() << std::endl;

    std::string base_url    = "https://cmsomsdet.cern.ch/tracker-resthub/query";
    std::string upload_url  = "https://cmsdca.cern.ch/trk_loader/trker/int2r";
    Resthub resthub(base_url);
    Query q = resthub.query(query);

//    RUN_EX(q.function("count"));
//    RUN_EX(q.cache_delete());
//    RUN_EX(q.cache());
    q.function("count");
    q.cache_delete();
    q.cache();
    return q.json2({}, 1, 1000);

}
