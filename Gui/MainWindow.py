# This Python file uses the following encoding: utf-8
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import uic


class MainWindow(QtWidgets.QMainWindow, uic.loadUiType('MainWindow.ui')[0]):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.setWindowTitle("Module Scorecard")

    def sayHi(self):
        print("ciao")
