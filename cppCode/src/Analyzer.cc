#include "headers/Analyzer.h"
#include "headers/Result.h"

#include <sstream>
#include <iostream>

#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>
#include <TH1.h>
#include <TGraphErrors.h>

//************************************************************************
Analyzer::Analyzer(Result* result){
  fResult = result;
  fResult->initializeNew();
}

//************************************************************************ 
float Analyzer::histYMean(TH1F* histo){
  return 1.0*histo->Integral()/(histo->GetXaxis()->GetXmax() - histo->GetXaxis()->GetXmin());
}

//************************************************************************ 
float Analyzer::histYStd(TH1F* histo, float mean){
  float variance = 0;
  for(int i=1; i<histo->GetNbinsX()+1; i++){
    variance += 1.0*(pow(histo->GetBinContent(i)-mean,2))/histo->GetNbinsX();
  }
  return pow(variance,0.5);
}

//************************************************************************                                                                                                   
std::vector<int> Analyzer::getOutliersHigh(TH1F* histo, float mean, float std, float threshold=5){
  // threshold gives the number of sigma away to be considered an outlier 

  std::vector<int> outliers;
  for(int i=1; i<histo->GetNbinsX()+1; i++){
    if( histo->GetBinContent(i) > mean + threshold*std ) outliers.push_back(i);
  }
  return outliers;
}

//************************************************************************                                                                                            
std::vector<int> Analyzer::getOutliersLow(TH1F* histo, float mean, float std, float threshold=5){
  // threshold gives the number of sigma away to be considered an outlier

  std::vector<int> outliers;
  for(int i=1; i<histo->GetNbinsX()+1; i++){
    if( histo->GetBinContent(i) < mean - threshold*std ) outliers.push_back(i);
  }
  return outliers;
}

//************************************************************************ 
TH1F* Analyzer::findHist(std::string histName, std::string component, TDirectory *dir){

  TH1F* histo;
    
  TIter keys( dir->GetListOfKeys() );
  TKey *dkey;
  while ( dkey = (TKey*)keys() ) {
    TObject *dobj = dkey->ReadObj();
    
    // If it's a directory, keep iterating        
    if ( dobj->IsA()->InheritsFrom( TDirectory::Class() ) ) {
      dir->cd(dobj->GetName());
      TDirectory* subDir = gDirectory;
      subDir->cd();
      histo = findHist(histName, component, subDir);
      if( histo != 0) return histo;
    }

    // If it's a histogram that comes the directory corresponding to the compnent we want, return it
    if( dobj->IsA()->InheritsFrom( TH1::Class() ) && dir->GetName() == component ) {
      std::string name = dobj->GetName();
      if ( name.find(histName) != std::string::npos) {
	histo = (TH1F*)dobj;
	histo->SetDirectory(0);
	return histo;
      }
    }
  } // end loop over keys

  return 0;
}

//************************************************************************
void Analyzer::getPartNoise(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

  std::string histName = "StripNoiseDistribution";
  TH1F* noiseHist = findHist(histName, componentName, dir);

  if( noiseHist == 0){
    std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
    return;
  }

  float mean = histYMean(noiseHist);
  float std = histYStd(noiseHist, mean);

  // Save data
  fResult->addNodeWithValue(parentNodeNames,"NoiseMean",std::to_string(mean));
  fResult->addNodeWithValue(parentNodeNames,"NoiseStd",std::to_string(std));

  std::vector<int> outliersLow = getOutliersLow(noiseHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"NoiseNOutliersLow",std::to_string(outliersLow.size()));
  std::string strOutliersLow = "";
  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"NoiseOutliersLow",strOutliersLow);

  std::vector<int> outliersHigh = getOutliersHigh(noiseHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"NoiseNOutliersHigh",std::to_string(outliersHigh.size()));
  std::string strOutliersHigh = "";
  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"NoiseOutliersHigh",strOutliersHigh);

}

//************************************************************************ 
void Analyzer::getPartPedestal(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

  std::string histName = "StripPedestalDistribution";
  TH1F* pedestalHist = findHist(histName, componentName, dir);

  if( !pedestalHist ){
    std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
    return;
  }

  float mean = histYMean(pedestalHist);
  float std = histYStd(pedestalHist, mean);

  // Save data
  fResult->addNodeWithValue(parentNodeNames,"PedestalMean",std::to_string(mean));
  fResult->addNodeWithValue(parentNodeNames,"PedestalStd",std::to_string(std));

  std::vector<int> outliersLow = getOutliersLow(pedestalHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"PedestalNOutliersLow",std::to_string(outliersLow.size()));
  std::string strOutliersLow = "";
  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"PedestalOutliersLow",strOutliersLow);

  std::vector<int> outliersHigh = getOutliersHigh(pedestalHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"PedestalNOutliersHigh",std::to_string(outliersHigh.size()));
  std::string strOutliersHigh = "";
  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"PedestalOutliersHigh",strOutliersHigh);

}

//************************************************************************ 
void Analyzer::getPartOffset(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

  std::string histName = "OffsetValues";
  TH1F* offsetHist = findHist(histName, componentName, dir);

  if( !offsetHist ){
    std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
    return;
  }

  float mean = histYMean(offsetHist);
  float std = histYStd(offsetHist, mean);

  // Save data
  fResult->addNodeWithValue(parentNodeNames,"OffsetMean",std::to_string(mean));
  fResult->addNodeWithValue(parentNodeNames,"OffsetStd",std::to_string(std));

  std::vector<int> outliersLow = getOutliersLow(offsetHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"OffsetNOutliersLow",std::to_string(outliersLow.size()));
  std::string strOutliersLow = "";
  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"OffsetOutliersLow",strOutliersLow);


  std::vector<int> outliersHigh = getOutliersHigh(offsetHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"OffsetNOutliersHigh",std::to_string(outliersHigh.size()));
  std::string strOutliersHigh = "";
  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"OffsetOutliersHigh",strOutliersHigh);

}

//************************************************************************ 
void Analyzer::getPartOccupancy(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

  std::string histName = "_Occupancy_";
  TH1F* occupancyHist = findHist(histName, componentName, dir);

  if( !occupancyHist ){
    std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
    return;
  }

  float mean = histYMean(occupancyHist);
  float std = histYStd(occupancyHist, mean);

  // Save data
  fResult->addNodeWithValue(parentNodeNames,"OccupancyMean",std::to_string(mean));
  fResult->addNodeWithValue(parentNodeNames,"OccupancyStd",std::to_string(std));

  std::vector<int> outliersLow = getOutliersLow(occupancyHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"OccupancyNOutliersLow",std::to_string(outliersLow.size()));
  std::string strOutliersLow = "";
  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"OccupancyOutliersLow",strOutliersLow);

  std::vector<int> outliersHigh = getOutliersHigh(occupancyHist,mean,std);
  fResult->addNodeWithValue(parentNodeNames,"OccupancyNOutliersHigh",std::to_string(outliersHigh.size()));
  std::string strOutliersHigh = "";
  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
  fResult->addNodeWithValue(parentNodeNames,"OccupancyOutliersHigh",strOutliersHigh);

}

//************************************************************************ 
void Analyzer::getPartIVScan(std::vector <std::string> parentNodeNames){

  std::cout << "Getting IVScan" << std::endl;
  fResult->addNodeWithValue(parentNodeNames,"IVScan","x");

  // NEEDS WRITTEN
}

//************************************************************************ 
void Analyzer::getPartVPlus(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){
  
  std::string histName = "VplusValue";
  TH1F* vplusHist = findHist(histName, componentName, dir);

  float vplus = vplusHist->GetBinContent(1);
  fResult->addNodeWithValue(parentNodeNames,"VPlus",std::to_string(vplus));
}
