import os
import ROOT

# Given a root file
# Find all TDirectories corresponding to a module test

class FileHandler:

    # Helper function recursively identifies directories containing key word
    def recursivelyFindDirs(self,dirContents,keyword):

        foundModuleTests = []
        for key in dirContents.keys():

            # Indicates module test directory
            if keyword in key:
                foundModuleTests += [key]

            # Continue descending
            else:
                subdirContents = {}
                for subdirKey in dirContents[key].GetListOfKeys():
                    subdirContents[key+"/"+subdirKey.GetName()] = dirContents[key].Get(subdirKey.GetName())
                foundModuleTests += self.recursivelyFindDirs(subdirContents,keyword)

        return foundModuleTests

    # Find all directories corresponding to a module test
    def findModuleTestDirs(self):

        fileContents = {}
        for key in self.fRootFile.GetListOfKeys():
            fileContents[key.GetName()] = key.ReadObj()

        moduleTestDirs = self.recursivelyFindDirs(fileContents,"OpticalGroup")

        return moduleTestDirs

    def openFile(self,fileName):
        self.fRootFile = ROOT.TFile.Open(fileName, "READ")

    def closeFile(self):
        self.fRootFile.Close()

    def getDirList(self):
        moduleTestDirs = []
        for directory in self.findModuleTestDirs():
            moduleTestDirs += [self.fRootFile.Get(directory)]
        return moduleTestDirs
        

