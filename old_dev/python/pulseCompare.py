import ROOT
from optparse import OptionParser
ROOT.gROOT.SetBatch(1)

execfile("basic_plotting.py")

parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")

basedir = "/eos/user/l/lhoryn/MagnetTest/"

flag = "endcap"

flies = {
    "barrel" : {
        "0T": {"file": basedir+"Results_0T/CommissioningFEH_2S_Skeleton_Run33/Commissioning.root"}, 
        "1.5T": {"file": basedir+"Results_1p5T/CommissioningFEH_2S_Skeleton_Run97/Commissioning.root"}, 
        "3T": {"file": basedir+"Results_3T/CommissioningFEH_2S_Skeleton_Run78/Commissioning.root"},
    },

    "endcap" : {

        "0T": {"file": basedir+"ResultsEC_0T/CommissioningFEH_2S_Skeleton_Run115/Commissioning.root"}, 
        "1.5T": {"file": basedir+"ResultsEC_1p5T/CommissioningFEH_2S_Skeleton_Run182/Commissioning.root"}, 
        "3T": {"file": basedir+"ResultsEC_3T/CommissioningFEH_2S_Skeleton_Run151/Commissioning.root"}, 
    }
}

files = flies[flag]

keys = ["0T","1.5T","3T"]

chips = ["Hybrid_0_Chip_0", "Hybrid_0_Chip_1", "Hybrid_0_Chip_2", "Hybrid_0_Chip_3", "Hybrid_0_Chip_4", "Hybrid_0_Chip_5","Hybrid_0_Chip_6", "Hybrid_0_Chip_7", "Hybrid_1_Chip_0", "Hybrid_1_Chip_1", "Hybrid_1_Chip_2", "Hybrid_1_Chip_3", "Hybrid_1_Chip_4", "Hybrid_1_Chip_5" , "Hybrid_1_Chip_6", "Hybrid_1_Chip_7"]

(options, args) = parser.parse_args()

c = ROOT.TCanvas()

hists = {}

for chip in chips: hists[chip] = []

for key in keys:
    if options.verbose: print "Parsing ", files[key]['file']
    ff = ROOT.TFile(files[key]['file'], "READ")
    for detector in ff.GetListOfKeys():
        d = detector.ReadObj()
        for board in d.GetListOfKeys():
            b = board.ReadObj()
            for opticalGroup in b.GetListOfKeys():
                o = opticalGroup.ReadObj()
                for hybrid in o.GetListOfKeys():
                    h = hybrid.ReadObj()
                    for chip in h.GetListOfKeys():
                        c = chip.ReadObj()
                        refname = hybrid.GetName() + "_" + chip.GetName()
                        print refname
                        for channel in c.GetListOfKeys():
                            
                            ## look for histograms, not directories
                            if "TH" not in channel.GetClassName(): continue
                            hname = channel.GetName()
                            if "PulseShapePerChip" in hname:
                                hist = channel.ReadObj()
                                hist.SetTitle(key)
                                hist.SetDirectory(0)
                                hists[refname].append(hist)
                                print len(hists[refname])

for name in hists:
    tmp = hists[name][0].Clone()
    tmp.Divide(hists[name][1])
    hists[name].append(tmp)
    
    tmp = hists[name][0].Clone()
    tmp.Divide(hists[name][2])
    hists[name].append(tmp)

    hists[name][1].SetLineColor(ROOT.kViolet)
    hists[name][3].SetLineColor(ROOT.kViolet)
    hists[name][2].SetLineColor(ROOT.kCyan-3)
    hists[name][4].SetLineColor(ROOT.kCyan-3)
    hists[name][1].SetMarkerColor(ROOT.kViolet)
    hists[name][3].SetMarkerColor(ROOT.kViolet)
    hists[name][2].SetMarkerColor(ROOT.kCyan-3)
    hists[name][4].SetMarkerColor(ROOT.kCyan-3)

    drawTwoPads(hists[name][0:3], hists[name][3:], "plots/pulseCompare_%s_%s.pdf"%(flag,name))
