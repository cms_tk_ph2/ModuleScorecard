#ifndef DatabaseHandler_h__
#define DatabaseHandler_h__

#include <map>
#include <string>
#include <vector>

class TDirectory;
struct Result;

//Stores results of all calculations
//Can dump to a new CSV file or a user-provided one -- to be updated with work on database
//TODO: make types in map configurable (currently string:float)
class DatabaseHandler
{
    public:
        DatabaseHandler(Result* &result);
        void addResult(std::string label, float value);
        
        void printResults();
        void dumpToCSV();

        void setIteration(std::string iteration) { fIteration = iteration;};
        void setModuleID(std::string moduleID) { fModuleID = moduleID;};
        void setOutputPath(std::string outputPath) { fOutputPath = outputPath;};
        
        float getResult(std::string label);
        std::map<std::string, float> getAllResults() { return fResults; };
        
        void readFromDatabase( std::string databasePath); 
    
    private:
        std::map<std::string, float> fResults;
        std::string fOutputPath = "";
        std::string fInputDBPath = "";
        std::string fModuleID = "";
        std::string fIteration = "";
        std::string fHeaderID = "moduleID";
        std::string fFileType = "";
        Result* fResult;


        std::vector<std::string> parseLine_CSV(std::string);
        void readInData_CSV();
};

#endif