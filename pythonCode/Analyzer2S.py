import Analyzer 

# Analysis code for 2S module results
class Analyzer2S(Analyzer.Analyzer):

    def analyze(self, inputDir, metadata):
        metadataOut = self.getMetaData(metadata)
        print("This is a 2S module")

        moduleName = "Dummy2SModule"
        self.fResult.updateResult([moduleName],{})

        moduleNoiseHistos = []
        modulePedestalHistos = []
        moduleOffsetHistos = []
        moduleOccupancyHistos = []

        # Loop over hybrids
        for hybridName in self.getHybrids(inputDir):
            print(hybridName)

            hybridDirectory = inputDir.Get(hybridName)

            # Get noise info for hybrid
            self.fResult.updateResult([moduleName,hybridName], self.getHybridNoise([self.getHybridNoiseHisto(hybridDirectory)]))

            # Loop over chips
            for chipName in self.getChips(hybridDirectory):
                print(chipName)

                chipDirectory = hybridDirectory.Get(chipName)
               
                # Get histograms of noise, pedestal, offest, occupancy for this chip
                chipNoiseHisto = self.getChipNoiseHisto(chipDirectory)
                chipPedestalHisto = self.getChipPedestalHisto(chipDirectory)
                chipOffsetHisto = self.getChipOffsetHisto(chipDirectory)
                chipOccupancyHisto = self.getChipOccupancyHisto(chipDirectory)

                # Calculate dictionary entries from histograms
                self.fResult.updateResult([moduleName,hybridName,chipName], self.getChipNoise([chipNoiseHisto]))
                self.fResult.updateResult([moduleName,hybridName,chipName], self.getChipPedestal([chipPedestalHisto]))
                self.fResult.updateResult([moduleName,hybridName,chipName], self.getChipOffset([chipOffsetHisto]))
                self.fResult.updateResult([moduleName,hybridName,chipName], self.getChipOccupancy([chipOccupancyHisto]))

                self.fResult.updateResult([moduleName,hybridName,chipName], self.getChipVplus(self.getChipVplusHisto(chipDirectory)))
                
                # Append chip histograms to list of histograms per module
                moduleNoiseHistos += [chipNoiseHisto]
                modulePedestalHistos += [chipPedestalHisto]
                moduleOffsetHistos += [chipOffsetHisto]
                moduleOccupancyHistos += [chipOccupancyHisto]

        self.fResult.updateResult([moduleName], self.getChipNoise(moduleNoiseHistos))
        self.fResult.updateResult([moduleName], self.getChipPedestal(modulePedestalHistos))
        self.fResult.updateResult([moduleName], self.getChipOffset(moduleOffsetHistos))
        self.fResult.updateResult([moduleName], self.getChipOccupancy(moduleOccupancyHistos))

        print(self.getResult().fResultDict)
        return self.getResult()

    def getHybridNoise(self, hybridHist):
        return self.getPartNoise(hybridHist)

    def getChipNoise(self, chipHist):
        return self.getPartNoise(chipHist)

    def getChipPedestal(self, chipHist):
        return self.getPartPedestal(chipHist)

    def getChipOffset(self, chipHist):
        return self.getPartOffset(chipHist)

    def getChipOccupancy(self, chipHist):
        return self.getPartOccupancy(chipHist)

    def getChipVplus(self, chipHist):

        returnDict = {}

        if chipHist.GetNbinsX() != 1:
            print("Unexpected number of bins in Vplus histogram. Setting to -999")
            returnDict["Vplus"] = -999
        else:
            returnDict["Vplus"] = chipHist.GetBinContent(1)

        return returnDict

    # Get the names of all hybrids in the directory
    def getHybrids(self,subDirectory):
        hybrids = []
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "Hybrid" in keyName:
                hybrids += [keyName]
        return hybrids

    # Get the hybrid noise histogram from a given directory
    def getHybridNoiseHisto(self, subDirectory):
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "_HybridStripNoiseDistribution_" in keyName:
                return subDirectory.Get(keyName)

    # Get the names of all chips in the directory
    def getChips(self,subDirectory):
        chips = []
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "Chip" in keyName:
                chips += [keyName]
        return chips

    # Get the chip noise histogram from a given directory
    def getChipNoiseHisto(self, subDirectory):
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "_StripNoiseDistribution_" in keyName:
                return subDirectory.Get(keyName)

    # Get the chip pedestal histogram from a given directory                                     
    def getChipPedestalHisto(self, subDirectory):
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "_StripPedestalDistribution_" in keyName:
                return subDirectory.Get(keyName)

    # Get the chip offset histogram from a given directory                                                                
    def getChipOffsetHisto(self, subDirectory):
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "_OffsetValues_" in keyName:
                return subDirectory.Get(keyName)

    # Get the chip occupancy histogram from a given directory                                                                                                
    def getChipOccupancyHisto(self, subDirectory):
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "_OccupancyAfterOffsetEqualization_" in keyName:
                return subDirectory.Get(keyName)

    # Get the chip vplus histogram from a given directory                                                                   
    def getChipVplusHisto(self, subDirectory):
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "_VplusValue_" in keyName:
                return subDirectory.Get(keyName)

    # Get the names of all channels in the directory                                                                            
    def getChannels(self,subDirectory):
        channels = []
        for key in subDirectory.GetListOfKeys():
            keyName = key.GetName()
            if "Channel" in keyName:
                channels += [keyName]
        return channels
