import ROOT
from optparse import OptionParser
from array import array
ROOT.gROOT.SetBatch(1)

execfile("basic_plotting.py")


def getNoise(hist, verbose):
    hh = hist.Clone()
     
    fit = ROOT.TF1("pol0","pol0",0,2000) 
    hh.Fit("pol0","RQ") #line 
    p0 = fit.GetParameter(0)
    err = fit.GetParError(0)

    return p0, err
    

def getBadChannels(hist, noise, verbose):
    badchans = 0
    diff = 0
    diff_s = 0
    for i in xrange(1, hist.GetNbinsX()+1):
       bad = False
       chanNoise = hist.GetBinContent(i)
       if chanNoise > (noise + 1): bad = True
       if chanNoise < (noise - 1): bad = True
       
       if bad:
           badchans+=1
           if verbose: print "-- Channel %i : Noise %2.4f"%(i, chanNoise)
           if abs(noise - chanNoise) > diff:
               diff = abs(noise - chanNoise)
               diff_s = noise - chanNoise 
    return badchans, diff_s




parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")

basedir = "/eos/user/l/lhoryn/MagnetTest/"

field = "3T"

orientations = ["barrel","endcap"]

files_f = {
    "3T": {
        "barrel":{
            "140": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run75/Hybrid.root"}, 
            "132": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run74/Hybrid.root"}, 
            "111": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run73/Hybrid.root"}, 
            "100": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run60/Hybrid.root"}, 
            "73": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run70/Hybrid.root"}, 
            "56": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run69/Hybrid.root"}, 
            "23": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run68/Hybrid.root"}, 
            "9": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run67/Hybrid.root"}, 
            "1": {"file": basedir+"Results_3T/triggerscan/FEH_2S_Skeleton_Run65/Hybrid.root"}, 
        },
        "endcap":{
            "140": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run134/Hybrid.root"}, 
            "132": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run141/Hybrid.root"}, 
            "111": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run140/Hybrid.root"}, 
            "100": {"file": basedir+"ResultsEC_3T/Trimming/FEH_2S_Skeleton_Run131/Hybrid.root"}, 
            "73": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run139/Hybrid.root"}, 
            "56": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run138/Hybrid.root"}, 
            "23": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run137/Hybrid.root"}, 
            "9": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run136/Hybrid.root"}, 
            "1": {"file": basedir+"ResultsEC_3T/triggerscan/FEH_2S_Skeleton_Run135/Hybrid.root"}, 
        }

    },
    "1p5T": {
        "barrel": {
            "140": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run93/Hybrid.root"}, 
            "132": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run92/Hybrid.root"}, 
            "111": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run90/Hybrid.root"}, 
            "100": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run80/Hybrid.root"}, 
            "73": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run89/Hybrid.root"}, 
            "56": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run88/Hybrid.root"}, 
            "23": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run86/Hybrid.root"}, 
            "9": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run83/Hybrid.root"}, 
            "1": {"file": basedir+"Results_1p5T/triggerscan/FEH_2S_Skeleton_Run82/Hybrid.root"},
        },
        "endcap": {
            "140": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run160/Hybrid.root"}, 
            "132": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run161/Hybrid.root"}, 
            "111": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run164/Hybrid.root"}, 
            "100": {"file": basedir+"ResultsEC_1p5T/Trimming/FEH_2S_Skeleton_Run159/Hybrid.root"}, 
            "73": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run165/Hybrid.root"}, 
            "56": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run167/Hybrid.root"}, 
            "23": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run168/Hybrid.root"}, 
            "9": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run170/Hybrid.root"}, 
            "1": {"file": basedir+"ResultsEC_1p5T/triggerscan/FEH_2S_Skeleton_Run171/Hybrid.root"},
        }
    },
    "0T" : {
        "barrel" : {
            "140": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run49/Hybrid.root"}, 
            "132": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run57/Hybrid.root"}, 
            "111": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run51/Hybrid.root"}, 
            "100": {"file": basedir+"Results_0T/Trimming/FEH_2S_Skeleton_Run35/Hybrid.root"}, 
            "73": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run50/Hybrid.root"}, 
            "56": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run56/Hybrid.root"}, 
            "23": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run52/Hybrid.root"}, 
            "9": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run43/Hybrid.root"}, 
            "1": {"file": basedir+"Results_0T/triggerscan/FEH_2S_Skeleton_Run45/Hybrid.root"},
        }
    }

}

files = files_f[field]
print files

rates = ["140", "132", "111", "100", "73", "56", "23","9","1"]
names = ["Hybrid_0_even", "Hybrid_1_even", "Hybrid_0_odd", "Hybrid_1_odd"]
measure = {
    "noise"  : {'label': "Noise [VCTH units]",                         'ymin': 7.5, 'ymax': 11.5},
    "badchan": {'label':  "# channels with noise +/- 1 from the mean", 'ymin': 0, 'ymax': 12},
    "bigdiff": {'label':  "largest difference from mean noise",        'ymin': -5, 'ymax': 5},
}

graphs = {
    "barrel" :{
        "Hybrid_0_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                },
        "Hybrid_0_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                },
        "Hybrid_1_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                },
        "Hybrid_1_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                },
    },
    "endcap" :{
        "Hybrid_0_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                },
        "Hybrid_0_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                },
        "Hybrid_1_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                },
        "Hybrid_1_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                },
    }
}




(options, args) = parser.parse_args()

for key in rates:
    print key
    for orient in orientations:
        if field == "0T" and orient == "endcap": continue
        print orient
        if options.verbose: print "Parsing ", files[orient][key]['file']
        ff = ROOT.TFile(files[orient][key]['file'], "READ")
        
        for detector in ff.GetListOfKeys():
            d = detector.ReadObj()
            for board in d.GetListOfKeys():
                b = board.ReadObj()
                for opticalGroup in b.GetListOfKeys():
                    op = opticalGroup.ReadObj()
                    for hybrid in op.GetListOfKeys():
                        h = hybrid.ReadObj()
                        for chip in h.GetListOfKeys():
                            
                            ## make hybrid-level histograms
                            if "TH" in chip.GetClassName():
                                hname = chip.GetName()
                                
                                if "HybridStripNoiseEvenDistribution" in hname or "HybridStripNoiseOddDistribution" in hname:
                                    k = "even" if "Even" in hname else "odd"
                                    hist = chip.ReadObj()
                                    if options.verbose: print "-- now processing ", hybrid.GetName(), k 
                                    ### Measure noise per side of hybrid
                                    print "filling", orient, hybrid.GetName(), k, key
                                     
                                    noise, error = getNoise(hist, options.verbose)
                                    graphs[orient][hybrid.GetName() + "_"+ k]['noise']['x'].append(float(key))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['noise']['y'].append(float(noise))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['noise']['err'].append(float(error))
                                    if options.verbose: print "-- Mean noise ", noise

                                    ### Count number of channels with high or low noise
                                    chans, bigdiff = getBadChannels(hist, noise, options.verbose) 
                                    graphs[orient][hybrid.GetName() + "_"+ k]['badchan']['x'].append(float(key))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['badchan']['y'].append(float(chans))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['badchan']['err'].append(float(0))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['bigdiff']['x'].append(float(key))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['bigdiff']['y'].append(float(bigdiff))
                                    graphs[orient][hybrid.GetName() + "_"+ k]['bigdiff']['err'].append(float(0))
                                    if options.verbose: print "-- Number of potentially bad channels", chans
                                    if options.verbose: print "-- Largest noise difference", bigdiff
                                    if options.verbose: print 
                                    
                                    print len(graphs[orient][hybrid.GetName() + "_"+ k]['noise']['x']) 
                                                               
                            



        ff.Close()


#print graphs

c = ROOT.TCanvas()
print "now trying to plot"

xerr = array('d')
for x in xrange(len(files["barrel"])): xerr.append(0)

for meas in measure:
    c.Clear()
    first = True
    leg = ROOT.TLegend(0.35,0.75,0.85,0.92)
    leg.SetFillStyle(0)
    #o = "barrel"
    for o in orientations:
        print field, orient
        #if field == "0T" and orient == "endcap": continue
        i=0
        for hybrid in names:
        
            print meas, o, hybrid, graphs[o][hybrid][meas]['x']
            print "making a graph", len(graphs[o][hybrid][meas]['y']), len(graphs[o][hybrid][meas]['x']), len(graphs[o][hybrid][meas]['err']), len(xerr)
            graphs[o][hybrid][meas]['graph'] = ROOT.TGraphErrors(len(files[o]), graphs[o][hybrid][meas]['x'], graphs[o][hybrid][meas]['y'], xerr, graphs[o][hybrid][meas]['err'])
            graphs[o][hybrid][meas]['graph'].GetHistogram().SetMaximum(measure[meas]['ymax']) 
            graphs[o][hybrid][meas]['graph'].GetHistogram().SetMinimum(measure[meas]['ymin'])
            graphs[o][hybrid][meas]['graph'].SetTitle(';Trigger Rate [kHz];%s'%measure[meas]['label']) 
            graphs[o][hybrid][meas]['graph'].SetLineWidth(2) 
            graphs[o][hybrid][meas]['graph'].SetLineColor(more_colors[i]) 
            graphs[o][hybrid][meas]['graph'].SetMarkerColor(more_colors[i]) 
            print "i set some stuff"
            i+=1
            
            leg.AddEntry(graphs[o][hybrid][meas]['graph'], hybrid + " " + o, 'p')
            
            if "endcap" in o:  
                graphs[o][hybrid][meas]['graph'].SetLineStyle(2) 
                graphs[o][hybrid][meas]['graph'].SetMarkerStyle(21) 
             
            if first: 
                graphs[o][hybrid][meas]['graph'].Draw('pela')
                first = False
            else:
                graphs[o][hybrid][meas]['graph'].Draw('pelsame')

    print "made it out!" 
    leg.SetNColumns(2)
    leg.Draw('same')
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    c.SaveAs("plots/rate_%sChange_summary_%s.pdf"%(meas, field))
#raw_input("..")


                             



