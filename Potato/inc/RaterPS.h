#ifndef RaterPS_h__
#define RaterPS_h__

#include "headers/Rater.h"
#include <map>

// Rater 2S
class RaterPS : public Rater {

 public:
  
  RaterPS();
  std::map<std::string, float> thresholdsGood;
  std::map<std::string, float> thresholdsOk;

  void setThresholds();
  void rate(std::string outputXmlName);

 private:
  
};

#endif
