import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import * 
from PyQt5.QtCore import * 
import datetime

from analyzeModuleMainWindow import Ui_analyzeModuleMW

class AnalyzeModuleMW(QMainWindow, Ui_analyzeModuleMW):
    def __init__(self, username, moduleID):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.fUsername = username
        self.fModuleID = moduleID

        self.consoleTB.append("user " + self.fUsername)
        self.consoleTB.append("module " + self.fModuleID)

    def closeEvent(self, event):
        text = self.textBrowser.toPlainText()
        n = datetime.datetime.now()
        string = str(n.year) + str(n.month) + str(n.day) + "_" + str(n.hour) + str(n.minute)
        with open("log_module%s_%s.txt"%(self.fModuleID, string) , "w") as f:
            f.write(text)

    def getModuleInfo(self):
        print ("run getModuleInfo")
        self.consoleTB.append("running getModuleInfo")
        # File handler stuff to identify module type etc etc
        # Call analysis script

    def uploadToDB(self):
        print ("run uploadTODB")
        self.consoleTB.append("running uploadToDB")
        # upload root file / histograms

