#ifndef Analyzer_h__
#define Analyzer_h__

#include "headers/Result.h"
#include <string>
#include <vector>
#include <TH1F.h>
#include <TDirectory.h>

// Analyzer
class Analyzer{

 public:

  Analyzer(Result* result);

  TH1F* findHist(std::string histName, std::string component, TDirectory* dir);
  float histYMean(TH1F* histo);
  float histYStd(TH1F* histo, float mean);

  std::vector <int> getOutliersHigh(TH1F* histo, float mean, float std, float threshold);
  std::vector <int> getOutliersLow(TH1F* histo, float mean, float std, float threshold);

  void getPartNoise(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
  void getPartPedestal(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
  void getPartOffset(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
  void getPartOccupancy(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
  void getPartIVScan(std::vector <std::string> parentNodeNames);
  void getPartVPlus(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
  
  Result* fResult;
  
};

#endif
