#/*===============================================================================
# * Potato: the Phase2 OuterTracker Analyzer of Test Outputs
# *
# * Copyright (C) 2021
# *
# * Authors:
# *
# * Lorenzo Uplegger   (FNAL)
# * Fabio Ravera       (FNAL)
# * Lesya Horyn        (FNAL)
# * Jennet Dickinson   (FNAL)
# * Karry DiPetrillo   (FNAL)
# * Irene Zoi          (FNAL)
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ================================================================================*/

# ================================================================================*/

if [ ${HOSTNAME} == "lenny01.fnal.gov" ]; then

  export BASEDATADIR=/data/TestBeam/2021_04_April_tracker

  #===== Local directories
  export POTATODIR=`pwd`
  export POTATOXMLDIR=${POTATODIR}/xml

  #===== Location of the ROOT components
  export ROOTSYS=/opt/local/root_v6.24.06
  export ROOTINCDIR=${ROOTSYS}/include
  export ROOTLIBDIR=${ROOTSYS}/lib

  #===== Location of the Qt components
  export QTDIR=/opt/local/qt/5.12.4/gcc_64
  export QTCREATORDIR=/opt/local/qt/Tools/QtCreator
  export QTLIBDIR=${QTDIR}/lib 
  export QTINCDIR=${QTDIR}/include 

  #===== Location of the BOOST components
  export BOOSTINCDIR=/usr/include/boost
  export BOOSTLIBDIR=/usr/lib64

  #===== Location of the PYTHON components
  export PYTHONINCDIR=/usr/include/python3.6m
  export PYTHONLIBDIR=/usr/lib64
  export PYTHONLIB=python3.6m

  #===== Location of the RESTHUB components
  export RESTHUBCPPDIR=${POTATODIR}/gitlab_resthub/clients/cpp

  export MODULE_FILES_DIR=${POTATODIR}/../test_files
  export XML_ANALYSIS_FILES_DIR=${POTATODIR}/XmlAnalysisOutputs

elif [ ${HOSTNAME} == "serf212a.desktop.utk.edu" ]; then

  echo "UTK - SERF 212"
  export BASEDATADIR=/data/TestBeam/2021_04_April_tracker

  #===== Local directories
  export POTATODIR=`pwd`
  export POTATOXMLDIR=${POTATODIR}/xml

  #===== Location of the ROOT components
  export ROOTSYS=/usr/ #/opt/local/root_v6.24.06
  export ROOTINCDIR=/usr/include/root  #${ROOTSYS}/include
  export ROOTLIBDIR=/usr/lib64/root  #${ROOTSYS}/lib

  #===== Location of the Qt components
  export QTDIR=/opt/Qt5.12.12/5.12.12/gcc_64
  export QTCREATORDIR=/opt/Qt5.12.12/Tools/QtCreator #/opt/local/qt/Tools/QtCreator
  export QTLIBDIR=${QTDIR}/lib 
  export QTINCDIR=${QTDIR}/include 

  #===== Location of the BOOST components
  export BOOSTINCDIR=/usr/include/boost
  export BOOSTLIBDIR=/usr/lib64

  #===== Location of the PYTHON components
  export PYTHONINCDIR=/usr/include/python3.6m
  export PYTHONLIBDIR=/usr/lib64
  export PYTHONLIB=python3.6m

  #===== Location of the RESTHUB components
  export RESTHUBCPPDIR=${POTATODIR}/gitlab_resthub/clients/cpp

  export MODULE_FILES_DIR=${POTATODIR}/../test_files
  export XML_ANALYSIS_FILES_DIR=${POTATODIR}/XmlAnalysisOutputs

fi

#===== Final PATH definitions
export PATH=${ROOTSYS}/bin:${QTDIR}/bin:${QTCREATORDIR}/bin:${PATH}
export LD_LIBRARY_PATH=${ROOTLIBDIR}:${POTATODIR}/plugins/libs
