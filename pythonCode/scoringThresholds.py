thresholds = {
    "hybridNBadStripsGood": 10,
    "hybridNBadStripsFair": 20,
    "chipNBadStripsGood": 2,
    "chipNBadStripsFair": 4,
    "noiseGood": 7.0,
    "noiseFair": 8.0,
}
