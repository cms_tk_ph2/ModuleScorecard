import ROOT
from optparse import OptionParser
from array import array
#ROOT.gROOT.SetBatch(1)

execfile("basic_plotting.py")


def getPedestal(hist, verbose):
    
    hh = hist.Clone()
    
    fit = ROOT.TF1("gaus","gaus",400,800) 
    hh.Fit("gaus","RQ")  
    mean = fit.GetParameter(1)
    sigma = fit.GetParError(2)

    return mean, sigma

parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")
(options, args) = parser.parse_args()

basedir = "/eos/user/l/lhoryn/MagnetTest/"

files = {
    "0": {"file": basedir+"Results_0T/Trimming/FEH_2S_Skeleton_Run35/Hybrid.root"}, 
    "1.5": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run79/Hybrid.root"}, 
    "3": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run60/Hybrid.root"}, 
    #"3T_2": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run61/Hybrid.root"}, 
    #"3T_3": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run64/Hybrid.root"}, 
    #"1p5T_2": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run80/Hybrid.root"}, 
    #"1p5T_3": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run81/Hybrid.root"}, 
}
keys = ["0", "1.5", "3"]

measure = {
    "pedestal"  : {'label': "Pedestal", 'ymin': 596, 'ymax': 601},
}
chips = ["Hybrid_0_Chip_0", "Hybrid_0_Chip_1", "Hybrid_0_Chip_2", "Hybrid_0_Chip_3", "Hybrid_0_Chip_4", "Hybrid_0_Chip_5","Hybrid_0_Chip_6", "Hybrid_0_Chip_7", "Hybrid_1_Chip_0", "Hybrid_1_Chip_1", "Hybrid_1_Chip_2", "Hybrid_1_Chip_3", "Hybrid_1_Chip_4", "Hybrid_1_Chip_5" , "Hybrid_1_Chip_6", "Hybrid_1_Chip_7"]

graphs = {
    "Hybrid_0_Chip_0": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
            },
    "Hybrid_0_Chip_1": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
            },
    "Hybrid_0_Chip_2": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
            },
    "Hybrid_0_Chip_3": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
            },
    "Hybrid_0_Chip_4": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[4]},
            },
    "Hybrid_0_Chip_5": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[5]},
            },
    "Hybrid_0_Chip_6": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[0]},
            },
    "Hybrid_0_Chip_7": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[1]},
            },
    "Hybrid_1_Chip_0": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[3]},
            },
    "Hybrid_1_Chip_1": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[3]},
            },
    "Hybrid_1_Chip_2": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[4]},
            },
    "Hybrid_1_Chip_3": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[5]},
            },
    "Hybrid_1_Chip_4": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[6]},
            },
    "Hybrid_1_Chip_5": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[7]},
            },
    "Hybrid_1_Chip_6": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[8]},
            },
    "Hybrid_1_Chip_7": {
              "pedestal"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': colors[9]},
            },
}


for key in keys:
    if options.verbose: print "Parsing ", files[key]['file']
    ff = ROOT.TFile(files[key]['file'], "READ")
    for detector in ff.GetListOfKeys():
        d = detector.ReadObj()
        for board in d.GetListOfKeys():
            b = board.ReadObj()
            for opticalGroup in b.GetListOfKeys():
                o = opticalGroup.ReadObj()
                for hybrid in o.GetListOfKeys():
                    h = hybrid.ReadObj()
                    for chip in h.GetListOfKeys():
                        # skip histograms
                        if "TH" in chip.GetClassName(): continue
                        c = chip.ReadObj()
                        for channel in c.GetListOfKeys():

                            ## parse chip-level histograms
                            if "TH" in channel.GetClassName():
                                hname = channel.GetName()
                                hist  = channel.ReadObj()

                                if "_Pedestal" in hname:

                                    if options.verbose: print "-- now processing ", hybrid.GetName(), chip.GetName() 
                                    if options.verbose: print "-- ", hname
                                    pede, sigma = getPedestal(hist, options.verbose)
                                    graphs[hybrid.GetName() + "_"+ chip.GetName()]['pedestal']['x'].append(float(key))
                                    graphs[hybrid.GetName() + "_"+ chip.GetName()]['pedestal']['y'].append(float(pede))
                                    graphs[hybrid.GetName() + "_"+ chip.GetName()]['pedestal']['err'].append(float(sigma))
                                    if options.verbose: print "--- pedestal ", pede, sigma




    ff.Close()




c = ROOT.TCanvas()
print "now trying to plot"

xerr = array('d')
for x in xrange(len(files)): xerr.append(0)

for meas in measure:
    c.Clear()
    leg = ROOT.TLegend(0.20,0.75,0.85,0.92)
    leg.SetFillStyle(0)
    leg.SetNColumns(4)
    first = True
    for chip in chips:
        graphs[chip][meas]['graph'] = ROOT.TGraphErrors(len(files), graphs[chip][meas]['x'], graphs[chip][meas]['y'], xerr, graphs[chip][meas]['err'])
        graphs[chip][meas]['graph'].GetHistogram().SetMaximum(measure[meas]['ymax']) 
        graphs[chip][meas]['graph'].GetHistogram().SetMinimum(measure[meas]['ymin'])
        graphs[chip][meas]['graph'].SetTitle(';Magnetic Field [T];%s'%measure[meas]['label']) 
        graphs[chip][meas]['graph'].SetLineWidth(2) 
        graphs[chip][meas]['graph'].SetLineColor(graphs[chip][meas]['color']) 
        graphs[chip][meas]['graph'].SetMarkerColor(graphs[chip][meas]['color']) 
        
        leg.AddEntry(graphs[chip][meas]['graph'], chip, 'p')
         
        if first: 
            graphs[chip][meas]['graph'].Draw('pela')
            first = False
        else:
            graphs[chip][meas]['graph'].Draw('pelsame')

        
    leg.Draw('same')
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    c.SaveAs("plots/%sChange_summary.pdf"%meas)
    raw_input("..")


                             







