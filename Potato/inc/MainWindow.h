/****************************************************************************
** Potato: the Phase2 OuterTracker Analyzer of Test Outputs
**
** Copyright (C) 2021
**
** Authors:
**
** Lorenzo Uplegger   (FNAL)
** Fabio Ravera       (FNAL)
** Lesya Horyn        (FNAL)
** Jennet Dickinson   (FNAL)
** Karry DiPetrillo   (FNAL)
** Irene Zoi          (FNAL)
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class ToolBar;
QT_FORWARD_DECLARE_CLASS(QMenu)

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    typedef QMap<QString, QSize> CustomSizeHintMap;

    explicit MainWindow(const CustomSizeHintMap &customSizeHints,
                        QWidget *parent = nullptr,
                        Qt::WindowFlags flags = 0);

public slots:
    void actionTriggered(QAction *action);
    void saveLayout();
    void loadLayout();
    void switchLayoutDirection();
    void setDockOptions();

    void createDockWidget();
    void destroyDockWidget(QAction *action);

    void about();

private:
    void setupToolBar();
    void setupMenuBar();
    void setupDockWidgets(const CustomSizeHintMap &customSizeHints);

    QList<ToolBar*> toolBars;
    QMenu *dockWidgetMenu;
    QMenu *mainWindowMenu;
    QList<QDockWidget *> extraDockWidgets;
    QMenu *destroyDockWidgetMenu;
};

#endif // MAINWINDOW_H
