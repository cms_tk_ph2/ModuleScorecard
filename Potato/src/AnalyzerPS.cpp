#include "Analyzer.h"
#include "AnalyzerPS.h"
#include "Result.h"

#include <sstream>
#include <iostream>

#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>
#include <TH1.h>
#include <TGraphErrors.h>

///////////////////////////////////////////////////////////////////////
AnalyzerPS::AnalyzerPS()
{
//  std::vector<std::string> partParentNames = {"DATA_SET","PART"};
//  result->addNodeWithValue(partParentNames,"KIND_OF_PART","PS Module");
//  result->addNodeWithValue(partParentNames,"BARCODE","module01");
}

///////////////////////////////////////////////////////////////////////
void AnalyzerPS::analyze(std::string fileName)
{
  std::cout << "I am analyzing" << std::endl;
  
  printMetaData();

}

///////////////////////////////////////////////////////////////////////
void AnalyzerPS::printMetaData(){
  std::cout << "This is PS module" << std::endl;
}
