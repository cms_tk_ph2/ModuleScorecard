#include "InputFileHandler.h"
#include "Result.h"
#include <sstream>
#include <iostream>
#include <fstream>


#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TH1.h>
#include <TObject.h>

///////////////////////////////////////////////////////////////////////

InputFileHandler::InputFileHandler()
{
    if(getenv("MODULE_FILES_DIR") == nullptr)
    {
        std::cout << "ERROR: Must source setup.sh file!" << std::endl;
        exit(EXIT_FAILURE);
    }
    fModuleFilesDir = getenv("MODULE_FILES_DIR");
    fModuleFilesDir += "/";
}

///////////////////////////////////////////////////////////////////////
void InputFileHandler::openFile (std::string fileName, std::string directory)
{
    if(directory == "")
        fResultFile = TFile::Open((fModuleFilesDir + fileName).c_str());
    else
        fResultFile = TFile::Open((directory + "/" + fileName).c_str());

}

///////////////////////////////////////////////////////////////////////
void InputFileHandler::closeFile(void)
{
    if(fResultFile != nullptr)
        fResultFile->Close();
    fResultFile = nullptr;
}

///////////////////////////////////////////////////////////////////////
TFile* InputFileHandler::getFile(void)
{
    return fResultFile;
}

///////////////////////////////////////////////////////////////////////
TH1* InputFileHandler::getHisto(std::string dirName, std::string histoName)
{
    std::cout << __PRETTY_FUNCTION__ << "Dir: " << dirName << " Histo: " << histoName << std::endl;
    TH1* histo = nullptr;
    TDirectory* directory = fResultFile->GetDirectory(dirName.c_str());
    TIter keys( directory->GetListOfKeys() );
    TKey *dkey;
    while ( dkey = (TKey*)keys() )
    {
      TObject *dobj = dkey->ReadObj();

      // If it's a histogram that comes the directory corresponding to the component we want, return it
      if( dobj->IsA()->InheritsFrom( TH1::Class() )) {
        std::string name = dobj->GetName();
        if ( name.find(histoName) != std::string::npos)
        {
            histo = (TH1*)dobj;
            break;
        }
      }
    } // end loop over keys
    return histo;
}


///////////////////////////////////////////////////////////////////////
std::string InputFileHandler::getModuleName(TDirectory *directory, std::string txtStart)
{
    std::string directoryPath = directory->GetPath();
    std::string modName = "";
    
    // C++ string parsing nonsense
    char *token = strtok(&directoryPath[0], "/");
    while (token != NULL)
    {
        
        std::string dirName = token;
        if ( dirName.find(txtStart) != std::string::npos) {
            //std::cout << token << std::endl;
            modName = dirName.erase(0,txtStart.size());
            //std::cout << "Module ID " << modName <<  std::endl;
        }
        token = strtok(NULL, "/");

    }

    return modName;
}
///////////////////////////////////////////////////////////////////////
std::string InputFileHandler::getIterationName(TDirectory *directory, std::string txtStart)
{
    std::string directoryPath = directory->GetPath();
    std::string iterName = "";
    
    // C++ string parsing nonsense
    char *token = strtok(&directoryPath[0], "/");
    while (token != NULL)
    {
        
        std::string dirName = token;
        if ( dirName.find(txtStart) != std::string::npos) {
            //std::cout << token << std::endl;
            iterName = dirName.erase(0,txtStart.size());
            //std::cout << "Iteration " << iterName <<  std::endl;
        }
        token = strtok(NULL, "/");

    }
    return iterName;
}


///////////////////////////////////////////////////////////////////////
// Recursive function to loop through directories 
void InputFileHandler::getNextDirectory(TDirectory* currentDirectory, Component* component)
{
    TIter keys( currentDirectory->GetListOfKeys() );
    TKey *dkey;
    while ( (dkey = (TKey*)keys()) )
    {
        TObject *dobj = dkey->ReadObj();
        
        
        if ( dobj->IsA()->InheritsFrom("TDirectory") )
        {
            
            TDirectory* nextDir       = (TDirectory*)dobj;
            Component*  nextComponent = component;

            // If directory name contains string of interest, append to results for analysis
            std::string name = dobj->GetName();
            std::cout << __PRETTY_FUNCTION__ << name << std::endl;
            std::cout << __PRETTY_FUNCTION__ << "Name: " << component->getName() << " Type: " << component->getType() << " Dir: " << component->getComponentDir() << std::endl;
            if ( name.find(fBoardName) != std::string::npos )
            {
                fCurrentBoardName = name;
            }
            else if ( name.find(fOpticalGroupName) != std::string::npos )
            {
                component->setProperties(name, fOpticalGroupName, "Detector/" + fCurrentBoardName);
            }
            else if ( name.find(fHybridName) != std::string::npos )
            {
                component->addComponent(name, fHybridName, component->getComponentDir());
                nextComponent = &component->getComponent(name);
            }
            else if ( name.find(fChipName) != std::string::npos )
            {
                component->addComponent(name, fChipName, component->getComponentDir());
                nextComponent = &component->getComponent(name);
            }

            InputFileHandler::getNextDirectory(nextDir, nextComponent);
        }

    }

}


///////////////////////////////////////////////////////////////////////
void InputFileHandler::makeResultTree(Result& result)
{
    // Recursively loop through file and identify which directories to analyze
    //     file/Detector/Board/OpticalGroup/Hybrid/Chip/Channel
    //     Identify modules with OpticalGroup_
    //     Identify iterations

    std::cout << __PRETTY_FUNCTION__ << "Making result tree! File: " << fResultFile << std::endl;
    if (fResultFile != nullptr)
    {
        InputFileHandler::getNextDirectory(fResultFile, &result.getOpticalGroup());
    }
    else
        std::cout << __PRETTY_FUNCTION__ << "There is no open root file!" << std::endl;

}
