import ROOT
from optparse import OptionParser
ROOT.gROOT.SetBatch(1)

## take in histogram and return # channels with noise +/- 1 from the central fit
def getBadChannels(hist, verbose):
    hh = hist.Clone()
     
    fit = ROOT.TF1("pol0","pol0",0,2000) 
    hh.Fit("pol0","RQ") #line 
    p0 = fit.GetParameter(0)
    err = fit.GetParError(0)
    
    if verbose: print "Mean noise: ", p0
    badchans = 0
    for i in xrange(1, hh.GetNbinsX()+1):
       bad = False
       if hh.GetBinContent(i) > (p0 + 1): bad = True
       if hh.GetBinContent(i) < (p0 - 1): bad = True
       
       if bad:
           badchans+=1
           if verbose: print "Channel %i : Noise %2.4f"%(i, hh.GetBinContent(i))

    return badchans


parser = OptionParser()
parser.add_option("-i", "--inputFile", dest="inputFile", help="input file")
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")

(options, args) = parser.parse_args()
fname = options.inputFile

f = ROOT.TFile(fname, "READ")
c = ROOT.TCanvas()

for detector in f.GetListOfKeys():
    d = detector.ReadObj()
    for board in d.GetListOfKeys():
        b = board.ReadObj()
        for opticalGroup in b.GetListOfKeys():
            o = opticalGroup.ReadObj()
            for hybrid in o.GetListOfKeys():
                h = hybrid.ReadObj()
                for chip in h.GetListOfKeys():

                    ## look for hybrid-level histograms
                    if "TH" not in chip.GetClassName(): continue
                    hname = chip.GetName()
                    
                    
                    ### look for bad strips
                    if "HybridStripNoiseEvenDistribution" in hname or "HybridStripNoiseOddDistribution" in hname:
                        k = "even" if "Even" in hname else "odd"
                        hist = chip.ReadObj()
                        bad = getBadChannels(hist, options.verbose)
                         
                        print "%s %s number of bad channels: %i"%(hybrid.GetName(),k, bad)
