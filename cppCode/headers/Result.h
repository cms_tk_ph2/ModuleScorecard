#ifndef Result_h__
#define Result_h__

#include <headers/pugixml.h>
#include <vector>

class TDirectory;

class Result{

 public:
  
  Result();
  TDirectory *directory;
  std::string moduleName;
  std::string iterationName;
  std::string testtype;

  void initializeNew();
  void initializeFromFile(std::string);

  void addNode(std::vector <std::string> parentNodeNames, std::string nodeName);
  void addNodeWithValue(std::vector <std::string> parentNodeNames, std::string nodeName, std::string value);
  void addAttribute(std::vector <std::string> parentNodeNames, std::string attName, std::string attValue);

  void printData();
  void writeToXml(std::string outputXmlFile);

 private:

  pugi::xml_document data;

};

#endif

