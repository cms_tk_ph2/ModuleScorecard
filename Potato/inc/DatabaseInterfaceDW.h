/****************************************************************************
** Potato: the Phase2 OuterTracker Analyzer of Test Outputs
**
** Copyright (C) 2021
**
** Authors:
**
** Lorenzo Uplegger   (FNAL)
** Fabio Ravera       (FNAL)
** Lesya Horyn        (FNAL)
** Jennet Dickinson   (FNAL)
** Karry DiPetrillo   (FNAL)
** Irene Zoi          (FNAL)
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef DATABASEINTERFACEDW_H
#define DATABASEINTERFACEDW_H

#include "BaseDW.h"
#include "gitlab_resthub/clients/cpp/include/Response.h"
#include "gitlab_resthub/clients/cpp/include/Request.h"
#include <curl/curl.h>


namespace Ui {
class DatabaseInterfaceDW;
}

class DatabaseInterfaceDW : public BaseDW
{
    Q_OBJECT

public:
    explicit DatabaseInterfaceDW(QMainWindow *parent = nullptr);
    ~DatabaseInterfaceDW();

private slots:
    void on_databaseInterfaceUploadPB_clicked();
    void on_databaseInterfaceGetRunNumberPB_clicked();

private:
    Ui::DatabaseInterfaceDW *ui;

private:
    resthub::Response runQuery(std::string query);
};

#endif // DATABASEINTERFACEDW_H
