#include "Analyzer.h"
#include "Result.h"

#include <sstream>
#include <iostream>

#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>
#include <TH1.h>
#include <TGraphErrors.h>

///////////////////////////////////////////////////////////////////////
Analyzer::Analyzer()
{
    //  fResult = result;
    //  fResult->initializeNew();
}

///////////////////////////////////////////////////////////////////////
Analyzer::~Analyzer()
{
}

///////////////////////////////////////////////////////////////////////
float Analyzer::histYMean(TH1F* histo){
    return 1.0*histo->Integral()/(histo->GetXaxis()->GetXmax() - histo->GetXaxis()->GetXmin());
}

///////////////////////////////////////////////////////////////////////
float Analyzer::histYStd(TH1F* histo, float mean){
    float variance = 0;
    for(int i=1; i<histo->GetNbinsX()+1; i++){
        variance += 1.0*(pow(histo->GetBinContent(i)-mean,2))/histo->GetNbinsX();
    }
    return pow(variance,0.5);
}

///////////////////////////////////////////////////////////////////////
std::vector<int> Analyzer::getOutliersHigh(TH1F* histo, float mean, float std, float threshold=5){
    // threshold gives the number of sigma away to be considered an outlier

    std::vector<int> outliers;
    for(int i=1; i<histo->GetNbinsX()+1; i++){
        if( histo->GetBinContent(i) > mean + threshold*std ) outliers.push_back(i);
    }
    return outliers;
}

///////////////////////////////////////////////////////////////////////
std::vector<int> Analyzer::getOutliersLow(TH1F* histo, float mean, float std, float threshold=5){
    // threshold gives the number of sigma away to be considered an outlier

    std::vector<int> outliers;
    for(int i=1; i<histo->GetNbinsX()+1; i++){
        if( histo->GetBinContent(i) < mean - threshold*std ) outliers.push_back(i);
    }
    return outliers;
}

///////////////////////////////////////////////////////////////////////
TH1F* Analyzer::findHist(std::string histName, std::string component, TDirectory *dir){

    TH1F* histo;
    
    TIter keys( dir->GetListOfKeys() );
    TKey *dkey;
    while ( dkey = (TKey*)keys() ) {
        TObject *dobj = dkey->ReadObj();

        // If it's a directory, keep iterating
        if ( dobj->IsA()->InheritsFrom( TDirectory::Class() ) ) {
            dir->cd(dobj->GetName());
            TDirectory* subDir = gDirectory;
            subDir->cd();
            histo = findHist(histName, component, subDir);
            if( histo != 0) return histo;
        }

        // If it's a histogram that comes the directory corresponding to the component we want, return it
        if( dobj->IsA()->InheritsFrom( TH1::Class() ) && dir->GetName() == component ) {
            std::string name = dobj->GetName();
            if ( name.find(histName) != std::string::npos) {
                histo = (TH1F*)dobj;
                histo->SetDirectory(0);
                return histo;
            }
        }
    } // end loop over keys

    return 0;
}

///////////////////////////////////////////////////////////////////////
void Analyzer::getComponentNoise(Component& component)
{
    std::vector<std::string> columnNames;
    columnNames.push_back("NoiseMean");
    columnNames.push_back("NoiseStd");
    columnNames.push_back("NumberOfNoiseOutliersLow");
    columnNames.push_back("NoiseOutliersLow");
    columnNames.push_back("NumberOfNoiseOutliersHigh");
    columnNames.push_back("NoiseOutliersHigh");

    DatabaseTable& noiseTable = component.setupTable("Noise",columnNames);

    std::string histoName = "StripNoiseDistribution";
    //"OpticalGroup_0/Hybrid_0/StripNoiseDistribution"
    //"OpticalGroup_0/Hybrid_0/Chip_0/StripNoiseDistribution"
    TH1F* noiseHist = (TH1F*)fInputFileHandler.getHisto(component.getComponentDir(), histoName);

    if( noiseHist == 0){
        std::cout << "Can't find histogram corresponding to " + histoName + " " + component.getName() << std::endl;
        return;
    }

    float mean = histYMean(noiseHist);
    float std  = histYStd(noiseHist, mean);

    //  // Save data
    noiseTable.setValue("NoiseMean",         std::to_string(mean));
    noiseTable.setValue("NoiseStd",          std::to_string(std));

    std::vector<int> outliersLow = getOutliersLow(noiseHist,mean,std);
    noiseTable.setValue("NumberOfNoiseOutliersLow", std::to_string(outliersLow.size()));
    std::string strOutliersLow = "";
    for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
    noiseTable.setValue("NoiseOutliersLow", strOutliersLow);

    std::vector<int> outliersHigh = getOutliersHigh(noiseHist,mean,std);
    noiseTable.setValue("NumberOfNoiseOutliersHigh", std::to_string(outliersHigh.size()));
    std::string strOutliersHigh = "";
    for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
    noiseTable.setValue("NoiseOutliersHigh", strOutliersHigh);

}

///////////////////////////////////////////////////////////////////////
void Analyzer::getComponentPedestal(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

    std::string histName = "StripPedestalDistribution";
    TH1F* pedestalHist = findHist(histName, componentName, dir);

    if( !pedestalHist ){
        std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
        return;
    }

    float mean = histYMean(pedestalHist);
    float std = histYStd(pedestalHist, mean);

    // Save data
    //  fResult->addNodeWithValue(parentNodeNames,"PedestalMean",std::to_string(mean));
    //  fResult->addNodeWithValue(parentNodeNames,"PedestalStd",std::to_string(std));

    //  std::vector<int> outliersLow = getOutliersLow(pedestalHist,mean,std);
    //  fResult->addNodeWithValue(parentNodeNames,"PedestalNOutliersLow",std::to_string(outliersLow.size()));
    //  std::string strOutliersLow = "";
    //  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
    //  fResult->addNodeWithValue(parentNodeNames,"PedestalOutliersLow",strOutliersLow);

    //  std::vector<int> outliersHigh = getOutliersHigh(pedestalHist,mean,std);
    //  fResult->addNodeWithValue(parentNodeNames,"PedestalNOutliersHigh",std::to_string(outliersHigh.size()));
    //  std::string strOutliersHigh = "";
    //  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
    //  fResult->addNodeWithValue(parentNodeNames,"PedestalOutliersHigh",strOutliersHigh);

}

///////////////////////////////////////////////////////////////////////
void Analyzer::getComponentOffset(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

    std::string histName = "OffsetValues";
    TH1F* offsetHist = findHist(histName, componentName, dir);

    if( !offsetHist ){
        std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
        return;
    }

    float mean = histYMean(offsetHist);
    float std = histYStd(offsetHist, mean);

    // Save data
    //  fResult->addNodeWithValue(parentNodeNames,"OffsetMean",std::to_string(mean));
    //  fResult->addNodeWithValue(parentNodeNames,"OffsetStd",std::to_string(std));

    //  std::vector<int> outliersLow = getOutliersLow(offsetHist,mean,std);
    //  fResult->addNodeWithValue(parentNodeNames,"OffsetNOutliersLow",std::to_string(outliersLow.size()));
    //  std::string strOutliersLow = "";
    //  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
    //  fResult->addNodeWithValue(parentNodeNames,"OffsetOutliersLow",strOutliersLow);


    //  std::vector<int> outliersHigh = getOutliersHigh(offsetHist,mean,std);
    //  fResult->addNodeWithValue(parentNodeNames,"OffsetNOutliersHigh",std::to_string(outliersHigh.size()));
    //  std::string strOutliersHigh = "";
    //  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
    //  fResult->addNodeWithValue(parentNodeNames,"OffsetOutliersHigh",strOutliersHigh);

}

///////////////////////////////////////////////////////////////////////
void Analyzer::getComponentOccupancy(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

    std::string histName = "_Occupancy_";
    TH1F* occupancyHist = findHist(histName, componentName, dir);

    if( !occupancyHist ){
        std::cout << "Can't find histogram corresponding to " + histName + " " + componentName << std::endl;
        return;
    }

    float mean = histYMean(occupancyHist);
    float std = histYStd(occupancyHist, mean);

    // Save data
    //  fResult->addNodeWithValue(parentNodeNames,"OccupancyMean",std::to_string(mean));
    //  fResult->addNodeWithValue(parentNodeNames,"OccupancyStd",std::to_string(std));

    //  std::vector<int> outliersLow = getOutliersLow(occupancyHist,mean,std);
    //  fResult->addNodeWithValue(parentNodeNames,"OccupancyNOutliersLow",std::to_string(outliersLow.size()));
    //  std::string strOutliersLow = "";
    //  for(int i=0; i<outliersLow.size(); i++){ strOutliersLow += std::to_string(outliersLow.at(i))+","; }
    //  fResult->addNodeWithValue(parentNodeNames,"OccupancyOutliersLow",strOutliersLow);

    //  std::vector<int> outliersHigh = getOutliersHigh(occupancyHist,mean,std);
    //  fResult->addNodeWithValue(parentNodeNames,"OccupancyNOutliersHigh",std::to_string(outliersHigh.size()));
    //  std::string strOutliersHigh = "";
    //  for(int i=0; i<outliersHigh.size(); i++){ strOutliersHigh += std::to_string(outliersHigh.at(i))+","; }
    //  fResult->addNodeWithValue(parentNodeNames,"OccupancyOutliersHigh",strOutliersHigh);

}

///////////////////////////////////////////////////////////////////////
void Analyzer::getComponentIVScan(std::vector <std::string> parentNodeNames){

    std::cout << "Getting IVScan" << std::endl;
    //  fResult->addNodeWithValue(parentNodeNames,"IVScan","x");

    // NEEDS WRITTEN
}

///////////////////////////////////////////////////////////////////////
void Analyzer::getComponentVPlus(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName){

    std::string histName = "VplusValue";
    TH1F* vplusHist = findHist(histName, componentName, dir);

    float vplus = vplusHist->GetBinContent(1);
    //  fResult->addNodeWithValue(parentNodeNames,"VPlus",std::to_string(vplus));
}
