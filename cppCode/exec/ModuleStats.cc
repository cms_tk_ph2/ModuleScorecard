#include "../headers/utils.h"
#include "../headers/cxxopts.h"
#include "../headers/InputFileHandler.h"
#include "../headers/Analyzer2S.h"
#include "../headers/Rater2S.h"

#include "TFile.h"
#include <iostream>
#include <fstream>
#include <sstream>

int main(int argc, char** argv){

  cxxopts::Options options("extract module features",  "extract module features");
  
  options.add_options()
    ("i,input"       , "Input file with Ph2_ACF directory structure",                                   cxxopts::value<std::string>())
    ("d,db"          , "Database file: either give me a csv to edit. If you don't I'll make a new one", cxxopts::value<std::string>()) 
    ("m,moduleID"    , "some value that represents the module. Stored as a string",                     cxxopts::value<std::string>()) 
    ("t,filetype"    , "indicates input file structure being used" ,                                    cxxopts::value<std::string>())
    ("h,help"        , "Print help");
  
  auto opts = options.parse(argc, argv);
  
  if(opts.count("help") ){
    std::cout << options.help() << std::endl;
    return 1;
  }
  
  if (! opts.count("input") ){
    std::cout << options.help() << std::endl;
    std::cout << "You  must provide an input file!" << std::endl;
    return 1;
  }
  
  std::string dateTime = getDateTimeString();
  //  std::cout << dateTime << std::endl;
  
  // Grab input file and prepare to grab some histograms from it 
  // for...as many files as needed? 
  // we decided to analyze one file at a time
  
  InputFileHandler inputFileHandler;
  // Define top level directory name for analysis
  inputFileHandler.setTopLevel("OpticalGroup"); 
  inputFileHandler.openFile(opts["input"].as<std::string>());
  
  // Get a vector of Results to analyze
  std::cout << " " << std::endl;
  std::cout << "Getting results to analyze" << std::endl;
  std::cout << " " << std::endl;
  
  std::vector<Result*> results = {};
  inputFileHandler.getResults(results);

  // Rater object can be re-used for each result in file
  Rater2S* rater = new Rater2S();
  
  // Loop over results contained in file
  int resultCounter = 0;  
  for (auto result : results){
    
    resultCounter++;
    std::cout << "Processing result " << resultCounter << std::endl;

    // Do analysis on module/iteration
    Analyzer2S *analyzer = new Analyzer2S(result);
    analyzer->analyze("testOutput.xml");

    // Rate the module you just analyzed
    rater->initializeFromResult(result);
    rater->rate("testOutputRated.xml");

    /*        
    // Take updated result and save to database
    DatabaseHandler *hybridResults = new DatabaseHandler(result);
    */    

  } // End loop over results
  std::cout << "Done processing " << resultCounter << " results" << std::endl;
  
}


