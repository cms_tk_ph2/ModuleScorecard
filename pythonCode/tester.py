import FileCollector
import FileHandler
import Analyzer2S
import Writer
import Scorer2S
import DatabaseHandler

collector_test1 = FileCollector.FileCollector()
collector_test1.findLocalFiles("../test_files/lenny.root")
print(collector_test1.fFileNames)

collector_test2 = FileCollector.FileCollector()
collector_test2.findLocalFiles("test.txt")
print(collector_test2.fFileNames)

collector_test3 = FileCollector.FileCollector()
collector_test3.findLocalFiles("../test_files/")
print(collector_test3.fFileNames)


outputXML ="testOutputFile.xml"
outputHTML="testOutputFile.html"

for testFile in collector_test1.fFileNames:
    print(testFile)
    fileHandler = FileHandler.FileHandler()
    fileHandler.openFile(testFile)
    for testDir in fileHandler.getDirList():

        analyzer = Analyzer2S.Analyzer2S()
        analyzer.analyze(testDir,"testmetadata")

        result = analyzer.getResult()

        writer = Writer.Writer()
        writer.writeResultToXML (result, outputXML )
        writer.writeResultToHTML(result, outputHTML)


        dbHandler = DatabaseHandler.DatabaseHandler()
        dbHandler.uploadXML(outputXML)

        # scoring done based on latest xml
        scorer = Scorer2S.Scorer2S()
        scorer.scoreModule(outputXML,"testmetadata")

        score = scorer.getModuleScore()

        writer.writeScoreToXML (score, outputXML )
        writer.writeScoreToHTML(score, outputHTML)

    fileHandler.closeFile()
