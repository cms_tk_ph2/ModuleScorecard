#ifndef ResultHandler_h__
#define ResultHandler_h__

#include <string>
#include <vector>

class TFile;
class TDirectory;
class TH1F;
struct Result;

// Computes results for a given module & iteration
class ResultHandler
{
    public:
        ResultHandler(Result* &result);
        void doAnalysis();
        void printResults();
        void updateResult(Result* &result);
    
    private:
        void addMeasurement(std::string label, float value);
        void noiseResult(std::string histName, std::string componentName, std::string component);
        bool grabHisto(TH1F* &histo, std::string histName, std::string component, TDirectory *d);

        Result* fResult;
};

#endif