#include "headers/DatabaseHandler.h"
#include "headers/Result.h"
#include <sstream>
#include <iostream>
#include <fstream>

// probably needs some cleaning
// save to xml instead of csv?

//***********************************************************************
DatabaseHandler::DatabaseHandler(Result* &result){
    fResult = result;
    fResults = fResult->measurements;
    fModuleID = fResult->moduleName;
    fIteration = fResult->iterationName;

}

//************************************************************************
//either update value in map, or add a new one 
void DatabaseHandler::addResult(std::string label, float value){
    
    std::map<std::string, float>::iterator it = fResults.find(label);
    
    if( it == fResults.end() )
        fResults.insert( std::pair<std::string, float>(label, value));
    else
        it->second = value;

}

//************************************************************************
//either update value in map, or add a new one 
float DatabaseHandler::getResult(std::string label){
    
    std::map<std::string, float>::iterator it = fResults.find(label);
    
    if( it == fResults.end() )
        return it->second;
    else
        return -999;

}

//************************************************************************
//dump to terminal
void DatabaseHandler::printResults(){

    std::cout << std::endl<< "PRINTING STORED RESULTS " << std::endl;
    std::cout << "module ID " << fModuleID << std::endl;
    for( auto it = fResults.begin(); it!= fResults.end(); it++){
        std::cout << it->first << " " << it->second << std::endl;
    }
}

//************************************************************************
void DatabaseHandler::dumpToCSV(){

   std::cout << "SAVING RESULTS TO CSV" << std::endl; 
    
   //make up an output file if one wasn't provideded.
   if(fOutputPath == ""){
       fOutputPath = "results_test";
       fOutputPath.append(".csv");
   }

   //check if the file is new.. i am sure there's a better way to do this
   bool newFile = false;
   std::ifstream ff(fOutputPath.c_str());
   if( !ff.good() ) newFile = true;
   
   std::cout << "is it a new file? " << newFile << std::endl;
   std::cout << "output path is  " << fOutputPath << std::endl;

   std::ofstream outFile;
   //if it's a new file we need to add the header
   if(newFile){
        outFile.open(fOutputPath.c_str(), std::ios::out);
        
        std::string header;
        header.append(fHeaderID);
        header.append(",");
        header.append("Iteration,"); // make not hardcoded
        for( auto it = fResult->measurements.begin(); it!= fResult->measurements.end(); it++){
            header.append(it->first);
            header.append(",");
        }
        outFile << header << std::endl;
   }
   //otherwise open in append mode
   else{
       std::cout << "appending! "<< std::endl;
       outFile.open(fOutputPath.c_str(), std::ios::app);
   }

   //in any case, write the data we've processed here
   std::string data;
   data.append(fModuleID);
   data.append(",");
   data.append(fIteration);
   data.append(",");
   for( auto it = fResults.begin(); it!= fResults.end(); it++){
       data.append(std::to_string(it->second));
       data.append(",");
   }
   std::cout << "data is " << data << std::endl;
   outFile << data << std::endl;

        
}

//************************************************************************
//take in database path, and then do something with it
//for now only handles CSV
void DatabaseHandler::readFromDatabase( std::string databasePath ){
    fInputDBPath = databasePath;
    
    if( fInputDBPath.find(".csv") != std::string::npos){
        readInData_CSV();
    }
    else{
        std::cout << "invalid input! Not reading anything" << std::endl;
    }
}

//************************************************************************
//put data from CSV file into a map -- I don't think this is the best way to do this, but I couldn't think of something else at the moment
//should be replaced with real database interface eventually
void DatabaseHandler::readInData_CSV(){
    
    std::ifstream csv_in(fInputDBPath);
    
    std::vector<std::string> headers;
    std::vector<std::string> data;
    std::string              line;
    
    while( std::getline(csv_in, line)){

        //check if this is a header row
        if( line.find(fHeaderID) != std::string::npos){
            headers = parseLine_CSV(line);
        }

        else if (line.find(fModuleID) != std::string::npos){
            data = parseLine_CSV(line);
        }
        else
            continue;
      
    }
    
    //this is the bad part, both vectors are ordered and the same size, so just dump them into a map
    for( int i=0; i < headers.size(); i++){
        if( headers[i] == fHeaderID){
            fModuleID = data[i];
            continue;
        }
        fResults.insert( std::pair<std::string, float>(headers[i], stof(data[i])) );
    }
}

//************************************************************************
//split a line from a CSV file into a vector of strings
std::vector<std::string> DatabaseHandler::parseLine_CSV(std::string line){
    std::vector<std::string> values;
    std::stringstream        lineStream(line);
    std::string              cell;

    while(std::getline(lineStream, cell, ',')){
        if( cell == " ") continue;
        values.push_back(cell);
    }

    return values;
}


