#ifndef Analyzer2S_h__
#define Analyzer2S_h__

#include "Analyzer.h"

// Analyzer
class Analyzer2S : public Analyzer {

 public:
  
  Analyzer2S();

  void analyze(std::string fileName) override;
  void printMetaData();

 private:
  
};

#endif
