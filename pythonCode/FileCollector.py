import os
import ROOT

# Given a textfile with list of root file names, or a directory of root files 
# Find all root files 

class FileCollector:

    def __init__(self):

        self.fFileNames = []

    def findLocalFiles(self,inputPath):

        # If input argument gives a directory                                                                           
        if os.path.isdir(inputPath):
            self.fFileNames = [inputPath+"/"+path for path in os.listdir(inputPath) if ".root" in path]

        # If input argument gives a file                                                                               
        elif os.path.isfile(inputPath):

            # Root file                                                                                                  
            if ".root" in inputPath:
                self.fFileNames = [inputPath]

            # Text file with list of root files                                                                            
            else:
                textFile = open(inputPath, 'r')
                self.fFileNames = [path.strip() for path in textFile.readlines() if ".root" in path]
                textFile.close()
        else:
            raise Exception("Input format not recognized")

        print("Found " + str(len(self.fFileNames)) + " root file(s)")

    def findDatabaseFiles(self):

        import DatabaseHandler

        print("To be implemented")
