#ifndef Analyzer_h__
#define Analyzer_h__

#include "Result.h"
#include "InputFileHandler.h"
#include <string>
#include <vector>
#include <TH1F.h>
#include <TDirectory.h>

// Analyzer
class Analyzer
{

public:

    Analyzer();
    ~Analyzer();

    virtual void analyze(std::string fileName) = 0;

    const Result& getResult(){return fResult;}

    TH1F* findHist(std::string histName, std::string component, TDirectory* dir);
    float histYMean(TH1F* histo);
    float histYStd(TH1F* histo, float mean);

    std::vector <int> getOutliersHigh(TH1F* histo, float mean, float std, float threshold);
    std::vector <int> getOutliersLow(TH1F* histo, float mean, float std, float threshold);

    void getComponentNoise    (Component& component);
    void getComponentPedestal (std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
    void getComponentOffset   (std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
    void getComponentOccupancy(std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);
    void getComponentIVScan   (std::vector <std::string> parentNodeNames);
    void getComponentVPlus    (std::vector <std::string> parentNodeNames, TDirectory* dir, std::string componentName);

protected:
    InputFileHandler fInputFileHandler;
    Result           fResult;

};

#endif
