#ifndef Rater2S_h__
#define Rater2S_h__

#include "Rater.h"
#include <map>

// Rater 2S
class Rater2S : public Rater {

 public:
  
  Rater2S();
  std::map<std::string, float> thresholdsGood;
  std::map<std::string, float> thresholdsOk;

  void setThresholds();
  void rate(std::string outputXmlName);

 private:
  
};

#endif
