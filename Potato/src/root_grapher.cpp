// Qt includes
#include "root_grapher.h"
#include "ui_root_grapher.h"
#include "qrootfile.h"

#include "map"
#include "QStandardItemModel"
#include "QAbstractItemModel"

// ROOT Includes
#include "TCanvas.h"
#include "TFrame.h"
#include "TH1F.h"
#include "TBrowser.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObject.h"


#include <iostream>
#include <cstring>
#include "bits/stdc++.h"

root_grapher::root_grapher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::root_grapher)
{
    // setting the ui
    ui->setupUi(this);

    // resetting the graph variable
    fPlot = 0;

    fRootFile = NULL;

}

root_grapher::~root_grapher()
{
    if(fRootFile) {
        delete fRootFile;
    }
    delete ui;
}


void root_grapher::plot_data(TObject *pGraph) {

    // delete the old plot if it exists
    if(fPlot != 0) ui->qcanvas->getCanvas()->Clear();

    // assign the object (to be able to delete later)
    fPlot = pGraph;

    // set our canvas settings (not fact if all is necessary)
    ui->qcanvas->getCanvas()->cd();
    ui->qcanvas->getCanvas()->GetFrame()->SetFillColor(42);
    ui->qcanvas->getCanvas()->GetFrame()->SetBorderMode(-1);
    ui->qcanvas->getCanvas()->GetFrame()->SetBorderSize(1);

    // draw the graph
    fPlot->Draw();

    // update the canvas
    this->updateCanvas();

}

void root_grapher::on_change_file_clicked()
{

    std::string filename = "lenny.root";
    const std::string Potato_var = "POTATODIR"; //Def of Potato DIR env var from setupPotato.sh=8
    const char * PotatoDIR = std::getenv( Potato_var.c_str() ); //Get pointer to env var
    if ( PotatoDIR == nullptr ) {
        std::cerr << "Environmental Variable '" << Potato_var << "' not defined." << std::endl; //Output error if env var not defined
    }
    else {
        const std::string PotatoPath = PotatoDIR; //Cast char* to string
        const std::string filePath = PotatoPath +  "/../test_files/" + filename;

        fRootFile = new QROOTFile(filePath);
        std::map<std::string , TObject *>* newMap = fRootFile->getMap();

        QStandardItemModel *model = new QStandardItemModel(this);
        int h=0;
        for(auto x: (*newMap)){
            QString str=QString::fromStdString(x.first);
            QStandardItem *item = new QStandardItem(str);
            model->setItem(h , item);
            model->index(h, 0);
            h++;
            }

        ui->histo_list->setModel(model);
        ui->histo_list->setEditTriggers(QAbstractItemView::NoEditTriggers);
        }


}

void root_grapher::on_histo_list_clicked(QModelIndex mIndex)
{
    QString str = mIndex.data().toString();
    std::string objName = str.toStdString();
    std::cout << objName << std::endl;

    plot_data(fRootFile->GetObject(objName));
}

void root_grapher::updateCanvas()
{
    // update the canvas
    ui->qcanvas->getCanvas()->Modified();
    ui->qcanvas->getCanvas()->Update();
}
