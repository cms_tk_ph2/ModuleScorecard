/****************************************************************************
** Potato: the Phase2 OuterTracker Analyzer of Test Outputs
**
** Copyright (C) 2021
**
** Authors:
**
** Lorenzo Uplegger   (FNAL)
** Fabio Ravera       (FNAL)
** Lesya Horyn        (FNAL)
** Jennet Dickinson   (FNAL)
** Karry DiPetrillo   (FNAL)
** Irene Zoi          (FNAL)
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef ANALYZERDW_H
#define ANALYZERDW_H

#include "BaseDW.h"
#include "root_grapher.h"

namespace Ui {
class AnalyzerDW;
}

class AnalyzerDW : public BaseDW
{
    Q_OBJECT

public:
    explicit AnalyzerDW(QMainWindow *parent = nullptr);
    ~AnalyzerDW();

private slots:
    void on_analyzerAnalyzePB_clicked();

private:
    Ui::AnalyzerDW *ui;
    root_grapher *Root_grapher;

};

#endif // ANALYZERDW_H
