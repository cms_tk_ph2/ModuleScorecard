#/*===============================================================================
# * Potato: the Phase2 OuterTracker Analyzer of Test Outputs
# *
# * Copyright (C) 2021
# *
# * Authors:
# *
# * Lorenzo Uplegger   (FNAL)
# * Fabio Ravera       (FNAL)
# * Lesya Horyn        (FNAL)
# * Jennet Dickinson   (FNAL)
# * Karry DiPetrillo   (FNAL)
# * Irene Zoi          (FNAL)
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ================================================================================*/

TEMPLATE = app
QT += widgets
requires(qtConfig(combobox))

QMAKE_CFLAGS_RELEASE   = -O3 $(CPLUSPLUSFLAGS)
QMAKE_CXXFLAGS_RELEASE = -O3 $(CPLUSPLUSFLAGS)

target.path   	     = ./
sources.path  	     = ./

ROOTLIBSFLAGS = $$system(${ROOTSYS}/bin/root-config --libs)
#message( Root libs: ($$ROOTLIBSFLAGS) )

MOC_DIR             += moc
UI_DIR              += ui
OBJECTS_DIR         += obj

HEADERS += \
	   inc/GraderDW.h                                 \
	   inc/ModuleSelectorDW.h                         \
	   inc/PyHelper.h                                 \
	   inc/Toolbar.h                                  \
	   inc/qrootfile.h                                \
	   inc/root_canvas.h                              \
	   plugins/customCheckBox/customCheckBox.h	      \
	   plugins/customComboBox/customComboBox.h	      \
	   plugins/customLineEdit/customLineEdit.h	      \
	   plugins/customSpinBox/customSpinBox.h	      \
	   plugins/customTableView/customTableView.h      \
	   plugins/customTableWidget/customTableWidget.h  \
	   plugins/customTextEdit/customTextEdit.h        \
	   $(RESTHUBCPPDIR)/include/Query.h               \
	   $(RESTHUBCPPDIR)/include/Request.h             \
	   $(RESTHUBCPPDIR)/include/Response.h            \
	   $(RESTHUBCPPDIR)/include/json.hpp              \
	   $(RESTHUBCPPDIR)/include/resthub.h             \
	   inc/MainWindow.h                               \
	   inc/AnalyzerDW.h                               \
	   inc/BaseDW.h                                   \
	   inc/DatabaseInterfaceDW.h                      \
	   inc/LoginDW.h                                  \
	   inc/ModuleSelectorDW.h                         \
	   inc/GraderDW.h                                 \
       inc/root_grapher.h                             \
	   inc/XmlWriter.h                                \
	   inc/Utils.h                                    \
	   inc/cxxopts.h                                  \
	   inc/Pugiconfig.h                               \
	   inc/Pugixml.h                                  \
	   inc/InputFileHandler.h                         \
	   inc/Rater.h                                    \
	   inc/Rater2S.h                                  \
	   inc/Result.h                                   \
	   inc/Analyzer.h                                 \
	   inc/Analyzer2S.h                               \
	   inc/AnalyzerPS.h

SOURCES += src/main.cpp                               \
       src/qrootfile.cpp                              \
       src/root_canvas.cxx                            \
       src/root_grapher.cpp                           \
	   plugins/customCheckBox/customCheckBox.cpp	  \
	   plugins/customComboBox/customComboBox.cpp	  \
	   plugins/customLineEdit/customLineEdit.cpp	  \
	   plugins/customSpinBox/customSpinBox.cpp	  \
	   plugins/customTableView/customTableView.cpp    \
	   plugins/customTableWidget/customTableWidget.cpp\
	   plugins/customTextEdit/customTextEdit.cpp      \
	   $(RESTHUBCPPDIR)/src/Query.cpp                 \
	   $(RESTHUBCPPDIR)/src/Request.cpp               \
	   $(RESTHUBCPPDIR)/src/Response.cpp              \
	   $(RESTHUBCPPDIR)/src/resthub.cpp               \
	   src/GraderDW.cpp                               \
	   src/MainWindow.cpp                             \
	   src/ModuleSelectorDW.cpp                       \
	   src/AnalyzerDW.cpp                             \
	   src/BaseDW.cpp                                 \
	   src/DatabaseInterfaceDW.cpp                    \
	   src/LoginDW.cpp                                \
	   src/Toolbar.cpp                                \
	   src/XmlWriter.cpp                              \
	   src/Utils.cpp                                  \
	   src/Pugixml.cpp                                \
	   src/InputFileHandler.cpp                       \
	   src/Rater.cpp                                  \
	   src/Rater2S.cpp                                \
	   src/Result.cpp                                 \
	   src/Analyzer.cpp                               \
	   src/Analyzer2S.cpp                             \
	   src/AnalyzerPS.cpp

FORMS   += ui/AnalyzerDW.ui                               \
	   ui/GraderDW.ui                                 \
	   ui/ModuleSelectorDW.ui                         \
	   ui/DatabaseInterfaceDW.ui                      \
           ui/LoginDW.ui                                  \
	   ui/root_grapher.ui

RESOURCES += Potato.qrc

INCLUDEPATH  	    += ./inc			          \
		       ./ui                               \
		       plugins/customCheckBox		  \
		       plugins/customComboBox		  \
		       plugins/customLineEdit		  \
		       plugins/customTextEdit		  \
		       plugins/customSpinBox		  \
		       plugins/customTableView	          \
		       plugins/customTableWidget	  \
		       $(PYTHONINCDIR)                    \
		       $(RESTHUBCPPDIR)/include           \
                       -pthread $(ROOTINCDIR)             \
                       /usr/include/root #New include for root grapher

LIBS                +=  -L$(PYTHONLIBDIR)                 \
			-l$(PYTHONLIB)                    \

LIBS                += $$ROOTLIBSFLAGS

LIBS         	    += -L$(POTATODIR)/plugins/libs	  \
		       -lcustomCheckBox 		  \
		       -lcustomComboBox 		  \
		       -lcustomLineEdit 		  \
		       -lcustomTextEdit 		  \
		       -lcustomTableView                  \
		       -lcustomTableWidget                \
		       -lcustomSpinBox

LIBS                += -L$(QTLIBDIR)                      \
#		       -lQt5Widgets                       \
#		       -lQt5Gui                           \
                       -lQt5Xml
#		       -lQt5Core                          \
#		       -lQt5OpenGL

LIBS                += -lcurl


DEPENDPATH          += . src inc

build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

# install
INSTALLS += target

extraclean.commands  = rm -rf Makefile                                                  \
		       tmp/*                                               	        \
		       *.pcm                                                            \
		       MakefileExpress

clean.depends        = extraclean

QMAKE_EXTRA_TARGETS += clean extraclean
