#ifndef XMLWRITER_H
#define XMLWRITER_H

#include "Pugixml.h"
#include "Result.h"
#include <map>
#include <vector>

class XmlWriter
{
public:
    XmlWriter();
    ~XmlWriter();

    void makeXmls(const Result& result);
    void initializeNew();
    void initializeFromFile(std::string);

    void addNode         (std::vector <std::string> parentNodeNames, std::string nodeName);
    void addNodeWithValue(std::vector <std::string> parentNodeNames, std::string nodeName, std::string value);
    void addAttribute    (std::vector <std::string> parentNodeNames, std::string attName, std::string attValue);

    void printData();
    void writeToXml(std::string outputXmlFile);

private:
    pugi::xml_document data;
};

#endif // XMLWRITER_H
