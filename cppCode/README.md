# ModuleScorecard

## Module Stats
ModuleStats computes things about the module based on output files from Ph2_ACF

Usage `ModuleStats -i [CALIBRATION FILE] -m [MODULE ID]`

You can also provide an output csv file to dump the information to. If you don't provide one, a new one will be made

### Module Rating
ModuleRater takes the output of ModuleStats and gives the module a score. It then appends that score to the same database you passed in.

Usage  `ModuleRater --db [DATABASE FILE (.csv)] -m [MODULE ID]`


### to do
* Clever way to handle menu of measurements
* Clever way to set thresholds for passage/failure
  * Maybe to both with a config file?

### To setup and compile
 ```
source setup.sh
mkdir build
cd build
cmake ..
cd ..
make -C build/ -j 60
```
