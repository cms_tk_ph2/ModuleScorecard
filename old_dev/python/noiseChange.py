import ROOT
from optparse import OptionParser
from array import array
#ROOT.gROOT.SetBatch(1)

execfile("basic_plotting.py")


def getNoise(hist, verbose):
    hh = hist.Clone()
     
    fit = ROOT.TF1("pol0","pol0",0,2000) 
    hh.Fit("pol0","RQ") #line 
    p0 = fit.GetParameter(0)
    err = fit.GetParError(0)

    return p0, err
    

def getBadChannels(hist, noise, verbose):
    badchans = 0
    diff = 0
    diff_s = 0
    for i in xrange(1, hist.GetNbinsX()+1):
       bad = False
       chanNoise = hist.GetBinContent(i)
       if chanNoise > (noise + 1): bad = True
       if chanNoise < (noise - 1): bad = True
       
       if bad:
           badchans+=1
           if verbose: print "-- Channel %i : Noise %2.4f"%(i, chanNoise)
           if abs(noise - chanNoise) > diff:
               diff = abs(noise - chanNoise)
               diff_s = noise - chanNoise 
    return badchans, diff_s

def getPedestal(hist, verbose):



parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")

basedir = "/eos/user/l/lhoryn/MagnetTest/"

files = {
    "0": {"file": basedir+"Results_0T/Trimming/FEH_2S_Skeleton_Run35/Hybrid.root"}, 
    "1.5": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run79/Hybrid.root"}, 
    "3": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run60/Hybrid.root"}, 
    #"3T_2": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run61/Hybrid.root"}, 
    #"3T_3": {"file": basedir+"Results_3T/Trimming/FEH_2S_Skeleton_Run64/Hybrid.root"}, 
    #"1p5T_2": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run80/Hybrid.root"}, 
    #"1p5T_3": {"file": basedir+"Results_1p5T/Trimming/FEH_2S_Skeleton_Run81/Hybrid.root"}, 
}

keys = ["0", "1.5", "3"]
names = ["Hybrid_0_even", "Hybrid_0_odd", "Hybrid_1_even", "Hybrid_1_odd"]
measure = {
    "noise"  : {'label': "Noise [VCTH units]",                         'ymin': 7, 'ymax': 11},
    "badchan": {'label':  "# channels with noise +/- 1 from the mean", 'ymin': 0, 'ymax': 12},
    "bigdiff": {'label':  "largest difference from mean noise",        'ymin': -5, 'ymax': 5},
}

graphs = {
    "Hybrid_0_even": {
              "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
              "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
              "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
            },
    "Hybrid_0_odd": {
              "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
              "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
              "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
            },
    "Hybrid_1_even": {
              "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
              "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
              "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
            },
    "Hybrid_1_odd": {
              "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
              "badchan": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
              "bigdiff": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
            },
}


(options, args) = parser.parse_args()


for key in keys:
    if options.verbose: print "Parsing ", files[key]['file']
    ff = ROOT.TFile(files[key]['file'], "READ")
    for detector in ff.GetListOfKeys():
        d = detector.ReadObj()
        for board in d.GetListOfKeys():
            b = board.ReadObj()
            for opticalGroup in b.GetListOfKeys():
                o = opticalGroup.ReadObj()
                for hybrid in o.GetListOfKeys():
                    h = hybrid.ReadObj()
                    for chip in h.GetListOfKeys():
                        
                        ## make hybrid-level histograms
                        if "TH" in chip.GetClassName():
                            hname = chip.GetName()
                            
                            if "HybridStripNoiseEvenDistribution" in hname or "HybridStripNoiseOddDistribution" in hname:
                                k = "even" if "Even" in hname else "odd"
                                hist = chip.ReadObj()
                                if options.verbose: print "-- now processing ", hybrid.GetName(), k 
                                ### Measure noise per side of hybrid
                                noise, error = getNoise(hist, options.verbose)
                                graphs[hybrid.GetName() + "_"+ k]['noise']['x'].append(float(key))
                                graphs[hybrid.GetName() + "_"+ k]['noise']['y'].append(float(noise))
                                graphs[hybrid.GetName() + "_"+ k]['noise']['err'].append(float(error))
                                if options.verbose: print "-- Mean noise ", noise

                                ### Count number of channels with high or low noise
                                chans, bigdiff = getBadChannels(hist, noise, options.verbose) 
                                graphs[hybrid.GetName() + "_"+ k]['badchan']['x'].append(float(key))
                                graphs[hybrid.GetName() + "_"+ k]['badchan']['y'].append(float(chans))
                                graphs[hybrid.GetName() + "_"+ k]['badchan']['err'].append(float(0))
                                graphs[hybrid.GetName() + "_"+ k]['bigdiff']['x'].append(float(key))
                                graphs[hybrid.GetName() + "_"+ k]['bigdiff']['y'].append(float(bigdiff))
                                graphs[hybrid.GetName() + "_"+ k]['bigdiff']['err'].append(float(0))
                                if options.verbose: print "-- Number of potentially bad channels", chans
                                if options.verbose: print "-- Largest noise difference", bigdiff
                                if options.verbose: print 
                             
                        ## make chip-level histograms   
                        else:
                            c.ReadObj()
                            for channel in c.GetListOfKeys():
                                
                                if "TH" in channel.GetClassName():
                                    hname = channel.GetName()
                                    hist = channel.ReadObj()
                                    
                                    if options.verbose: print "--- now processing", chip.GetName()
                                    if "Pedestal" in hname:
                                        pedestal = getPedestal(hist)
                                                               
                            



    ff.Close()


c = ROOT.TCanvas()
print "now trying to plot"

xerr = array('d')
for x in xrange(len(files)): xerr.append(0)

for meas in measure:
    c.Clear()
    leg = ROOT.TLegend(0.60,0.75,0.85,0.92)
    leg.SetFillStyle(0)
    first = True
    for hybrid in names:
        graphs[hybrid][meas]['graph'] = ROOT.TGraphErrors(len(files), graphs[hybrid][meas]['x'], graphs[hybrid][meas]['y'], xerr, graphs[hybrid][meas]['err'])
        graphs[hybrid][meas]['graph'].GetHistogram().SetMaximum(measure[meas]['ymax']) 
        graphs[hybrid][meas]['graph'].GetHistogram().SetMinimum(measure[meas]['ymin'])
        graphs[hybrid][meas]['graph'].SetTitle(';Magnetic Field [T];%s'%measure[meas]['label']) 
        graphs[hybrid][meas]['graph'].SetLineWidth(2) 
        graphs[hybrid][meas]['graph'].SetLineColor(graphs[hybrid][meas]['color']) 
        graphs[hybrid][meas]['graph'].SetMarkerColor(graphs[hybrid][meas]['color']) 
        
        leg.AddEntry(graphs[hybrid][meas]['graph'], hybrid, 'p')
         
        if first: 
            graphs[hybrid][meas]['graph'].Draw('pela')
            first = False
        else:
            graphs[hybrid][meas]['graph'].Draw('pelsame')

        
    leg.Draw('same')
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    c.SaveAs("plots/%sChange_summary.pdf"%meas)
    raw_input("..")


                             



