import ROOT
from optparse import OptionParser
from datetime import datetime
ROOT.gROOT.SetBatch(1)


def getNoise(hist, verbose):
    hh = hist.Clone()
     
    fit = ROOT.TF1("pol0","pol0",0,2000) 
    hh.Fit("pol0","RQ") #line 
    p0 = fit.GetParameter(0)
    err = fit.GetParError(0)

    return p0, err


def getNoiseHist(hist, hybridName, verbose):
    tmp = ROOT.TH1F(hybridName+"Noise",hybridName+"Noise", 100, 7, 11)
    for i in xrange(1, hist.GetNbinsX()+1):
        tmp.Fill(hist.GetBinContent(i))
    return tmp

    

def getBadChannels(hist, noise, verbose):
    badchans = 0
    diff = 0
    diff_s = 0
    for i in xrange(1, hist.GetNbinsX()+1):
       bad = False
       chanNoise = hist.GetBinContent(i)
       if chanNoise > (noise + 1): bad = True
       if chanNoise < (noise - 1): bad = True
       
       if bad:
           badchans+=1
           if verbose: print "-- Channel %i : Noise %2.4f"%(i, chanNoise)
           if abs(noise - chanNoise) > diff:
               diff = abs(noise - chanNoise)
               diff_s = noise - chanNoise 
    return badchans, diff_s




parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")
parser.add_option("-i", "--inputFile",  dest="inputFile", help="If true, prints out bad channels and their noise")
parser.add_option("-o", "--outputFile",  dest="outputFile", help="a CSV to append to, if not provided a new csv will be created")


results = {}
results["full"] = {}
results["full"]["badChannels"] = 0

(options, args) = parser.parse_args()

if options.verbose: print "Parsing ", files[key]['file']
ff = ROOT.TFile(options.inputFile, "READ")
for detector in ff.GetListOfKeys():
    d = detector.ReadObj()
    for board in d.GetListOfKeys():
        b = board.ReadObj()
        for opticalGroup in b.GetListOfKeys():
            o = opticalGroup.ReadObj()
            for hybrid in o.GetListOfKeys():
                h = hybrid.ReadObj()
                for chip in h.GetListOfKeys():
                    
                    ## make hybrid-level histograms
                    if "TH" in chip.GetClassName():
                        hname = chip.GetName()
                        
                        if "HybridStripNoiseEvenDistribution" in hname or "HybridStripNoiseOddDistribution" in hname:
                            k = "even" if "Even" in hname else "odd"
                            hist = chip.ReadObj()
                            if hybrid.GetName()+k not in results: results[hybrid.GetName()+k] = {}

                            if options.verbose: print "-- now processing ", hybrid.GetName(), k 
                            ### Measure noise per side of hybrid
                            noise, error = getNoise(hist, options.verbose)
                            noiseHist = getNoiseHist(hist,hybrid.GetName()+str(noise),  options.verbose)
                            rms = noiseHist.GetRMS()
                            results[hybrid.GetName()+k]["noise"] = noise
                            results[hybrid.GetName()+k]["noiseRMS"] = rms
                            if options.verbose: print "-- Mean noise ", noise

                            ### Count number of channels with high or low noise
                            chans, bigdiff = getBadChannels(hist, noise, options.verbose) 
                            results["full"]["badChannels"] += chans
                            if options.verbose: print "-- Number of potentially bad channels", chans
                            if options.verbose: print "-- Largest noise difference", bigdiff
                            if options.verbose: print 
                         
ff.Close()


if options.outputFile is not None:
    output = options.outputFile
    opt = 'a'
else:
    output = "results_%s.csv"%datetime.now().strftime("%d%m_%H%M%S")
    opt = 'w'


with open(output, opt) as f:
    if options.outputFile is None:
        f.write("module, noise_h0odd, noise_h0even, noise_h1odd, noise_h1even, badChannels\n")
    f.write("%s, %s, %s, %s, %s, %s\n"%(options.inputFile.split("/")[-1].strip(".root"), results["Hybrid_0odd"]["noise"], results["Hybrid_0even"]["noise"],
    results["Hybrid_1odd"]["noise"], results["Hybrid_1even"]["noise"], results["full"]["badChannels"]))


                     



