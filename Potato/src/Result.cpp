#include "Result.h"

///////////////////////////////////////////////////////////////////////
Result::Result()
{

}

///////////////////////////////////////////////////////////////////////
Result::~Result()
{
}

///////////////////////////////////////////////////////////////////////
void Result::clear()
{
    for(const auto& constHybrid: fOpticalGroup.getComponents())
    {
        Component& hybrid = fOpticalGroup.getComponent(constHybrid.first);
        for(const auto& constChip: constHybrid.second.getComponents())
        {
            Component& chip = hybrid.getComponent(constChip.first);
            chip.clear();
        }
        hybrid.clear();
    }
    fOpticalGroup.clear();
}

///////////////////////////////////////////////////////////////////////
void Result::addHybrid(std::string componentName)
{
    fOpticalGroup.addComponent(componentName, "Hybrid", fOpticalGroup.getComponentDir());
}

///////////////////////////////////////////////////////////////////////
void Result::addChip(Component& hybrid, std::string componentName)
{
    hybrid.addComponent(componentName, "Chip", hybrid.getComponentDir());
}

///////////////////////////////////////////////////////////////////////
void Result::addTable(Component& component, std::string tableName)
{
    component.addTable(tableName);
}

///////////////////////////////////////////////////////////////////////
Component& Result::getOpticalGroup(void)
{
    return fOpticalGroup;
}

///////////////////////////////////////////////////////////////////////
const std::map<std::string, Component>& Result::getHybrids(void) const
{
    return fOpticalGroup.getComponents();
}

///////////////////////////////////////////////////////////////////////
const std::map<std::string, Component>& Result::getChips(Component& hybrid) const
{
    return hybrid.getComponents();
}

///////////////////////////////////////////////////////////////////////
void Result::setTableValue(Component& component, std::string tableName, std::string column, std::string value)
{
    component.getTable(tableName).setValue(column, value);
}
