# This Python file uses the following encoding: utf-8
import sys
import os

from MainWindow import MainWindow
from PyQt5 import QtWidgets
#from PyQt5.QtUiTools import QUiLoader
#from PyQt5.QtCore import QFile


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec_())
