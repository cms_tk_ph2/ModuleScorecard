class Score():

    def __init__(self):
        self.fScoreDict = {}
        self.fPart = 0
        self.fRun = 0

    def updateScore(self, keyword, value):
        # example, updateScore("AverageNoise",4.2)
        self.fScoreDict[keyword] = value
