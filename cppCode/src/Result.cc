#include "headers/Result.h"

#include <iostream>
#include <fstream>

//************************************************************************
Result::Result(){}

void Result::initializeNew(){

  pugi::xml_node root = data.append_child("ROOT");
  pugi::xml_node header = root.append_child("HEADER");
  pugi::xml_node type = header.append_child("TYPE");
  pugi::xml_node run = header.append_child("RUN");
  pugi::xml_node dataset = root.append_child("DATA_SET");

  std::vector<std::string> typeParentNames;
  typeParentNames.push_back("HEADER");
  typeParentNames.push_back("TYPE");

  addNodeWithValue(typeParentNames,"EXTENSION_TABLE_NAME","TEST_ASSEMBLY_SMMRY");
  addNodeWithValue(typeParentNames,"NAME","Tracker Module Assembly Summary Data");

  std::vector<std::string> runParentNames;
  runParentNames.push_back("HEADER");
  runParentNames.push_back("RUN");

  addAttribute(runParentNames,"mode","AUTO_INC_NUMBER");
  addNodeWithValue(runParentNames,"RUN_TYPE","mod_final");
  addNodeWithValue(runParentNames,"LOCATION","Awesome Assembly Site");
  addNodeWithValue(runParentNames,"INITIATED_BY_USER","Awesome Operator");
  addNodeWithValue(runParentNames,"RUN_BEGIN_TIMESTAMP","time");
  addNodeWithValue(runParentNames,"COMMENT_DESCRIPTION","Blah blah");

  std::vector<std::string> datasetParentNames;
  datasetParentNames.push_back("DATA_SET");

  addNodeWithValue(datasetParentNames,"COMMENT_DESCRIPTION","Comment for the dataset");
  addNodeWithValue(datasetParentNames,"VERSION","X");
  addNode(datasetParentNames,"PART");
  addNode(datasetParentNames,"DATA");

}

//************************************************************************   
void Result::initializeFromFile(std::string inputXmlFile=""){
  // do stuff
}

//************************************************************************                                                                                                      
void Result::addNode(std::vector <std::string> parentNodeNames, std::string nodeName){  

  pugi::xml_node node = data.child("ROOT");

  for(int i=0; i<parentNodeNames.size(); i++){
    node = node.child(parentNodeNames.at(i).c_str());
  }
  node.append_child(nodeName.c_str());
}

//************************************************************************
void Result::addNodeWithValue(std::vector <std::string> parentNodeNames, std::string nodeName, std::string value){

  pugi::xml_node node = data.child("ROOT");

  for(int i=0; i<parentNodeNames.size(); i++){
    node = node.child(parentNodeNames.at(i).c_str());
  }

  pugi::xml_node nodeChild = node.append_child(nodeName.c_str());
  nodeChild.append_child(pugi::node_pcdata).set_value(value.c_str());

}

//************************************************************************                                                                                                      
void Result::addAttribute(std::vector <std::string> parentNodeNames, std::string attName, std::string attValue){

  pugi::xml_node node = data.child("ROOT");

  for(int i=0; i<parentNodeNames.size(); i++){
    node = node.child(parentNodeNames.at(i).c_str());
  }

  node.append_attribute(attName.c_str()) = attValue.c_str();

}

//************************************************************************ 
void Result::printData(){
  data.print(std::cout);
}

//************************************************************************ 
void Result::writeToXml(std::string outputXmlFile){
  data.save_file(outputXmlFile.c_str());
}

