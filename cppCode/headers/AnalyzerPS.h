#ifndef AnalyzerPS_h__
#define AnalyzerPS_h__

#include "headers/Analyzer.h"

// Analyzer
class AnalyzerPS : public Analyzer {

 public:
  
  AnalyzerPS(Result* result);

  void analyze();
  void printMetaData();

 private:
  
};

#endif
