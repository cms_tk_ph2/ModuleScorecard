#ifndef Result_h__
#define Result_h__

#include <map>

class TDirectory;

struct Result
{
    
    std::string moduleName;
    std::string iterationName;
    std::string testtype;
    TDirectory *directory;

    // Would be better if we could do this for multiple data formats, eg list of bad strips?
    std::map<std::string,float> measurements;

    // Burn-in box specific summary
    // * temperature
    // * power supply



};
 
#endif