#ifndef Analyzer2S_h__
#define Analyzer2S_h__

#include "headers/Analyzer.h"

// Analyzer
class Analyzer2S : public Analyzer {

 public:
  
  Analyzer2S(Result* result);

  void analyze(std::string outputXmlName);
  void printMetaData();

 private:
  
};

#endif
