/****************************************************************************
** Potato: the Phase2 OuterTracker Analyzer of Test Outputs
**
** Copyright (C) 2021
**
** Authors:
**
** Lorenzo Uplegger   (FNAL)
** Fabio Ravera       (FNAL)
** Lesya Horyn        (FNAL)
** Jennet Dickinson   (FNAL)
** Karry DiPetrillo   (FNAL)
** Irene Zoi          (FNAL)
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include "AnalyzerDW.h"
#include "ui_AnalyzerDW.h"
#include "root_grapher.h"
#include <QApplication>

// ROOT Includes
#include <TApplication.h>
#include <TSystem.h>

#include "Utils.h"
#include "cxxopts.h"
#include "InputFileHandler.h"
#include "Analyzer2S.h"
#include "AnalyzerPS.h"
#include "XmlWriter.h"

#include "TFile.h"
#include <iostream>
#include <fstream>
#include <sstream>


///////////////////////////////////////////////////////////////////////
AnalyzerDW::AnalyzerDW(QMainWindow *parent) :
    BaseDW("Analyzer", parent),
    ui(new Ui::AnalyzerDW)
{
    ui->setupUi(this);
}

///////////////////////////////////////////////////////////////////////
AnalyzerDW::~AnalyzerDW()
{
    delete ui;
}

///////////////////////////////////////////////////////////////////////
void AnalyzerDW::on_analyzerAnalyzePB_clicked()
{

    if(getenv("XML_ANALYSIS_FILES_DIR") == nullptr)
    {
        std::cout << "ERROR: Must source setup.sh file!" << std::endl;
        return;
    }
    std::string xmlAnalysisOutputFilesDir = getenv("XML_ANALYSIS_FILES_DIR");

    std::vector<std::string> fileList;
    fileList.push_back("2S_18_4_FNL_007_25-02-22_11h43m50s.root");

    Analyzer2S analyzer2S;
    AnalyzerPS analyzerPS;
    Analyzer*  analyzer;
    XmlWriter  theXmlWriter;

    // Loop over file list
    for (auto& file : fileList)
    {
        std::cout << "Processing file " << file << std::endl;

        // Do analysis on module/iteration
        if(file.find("2S", 0, 2) != std::string::npos)
            analyzer = &analyzer2S;
        if(file.find("PS", 0, 2) != std::string::npos)
            analyzer = &analyzerPS;
        analyzer->analyze(file);
        theXmlWriter.makeXmls(analyzer->getResult());
    }

    // Should go in a different button    
    // Root_grapher = new root_grapher(this);
    // Root_grapher->show();

}


