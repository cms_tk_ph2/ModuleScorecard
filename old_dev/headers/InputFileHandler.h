#ifndef InputFileHandler_h__
#define InputFileHandler_h__

#include <string>
#include <vector>

class TFile;
class TDirectory;
struct Result;

// Manages the files with the results
// Handles complexity of different file structures
class InputFileHandler
{
    public:
        InputFileHandler();
        void openFile (std::string fileName);
        bool getFile  (TFile* &file);
        bool setTopLevel(std::string topLevelName);
        bool getResults(std::vector<Result*> &resultsToAnalyze);
        void closeFile(void);
    
    private:
        TFile* fResultFile = nullptr;
        std::string fTopLevelName = "OpticalGroup"; // string to find top level for analysis
        std::vector<Result*> fResultsToAnalyze = {};
        void getNextDirectory(TDirectory* currentDirectory);
        std::string getModuleName(TDirectory* directory, std::string txtStart);
        std::string getIterationName(TDirectory* directory, std::string txtStart);
};

#endif