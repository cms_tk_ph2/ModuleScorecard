import ROOT
from optparse import OptionParser
from array import array
from glob import glob
ROOT.gROOT.SetBatch(1)

execfile("basic_plotting.py")
execfile("allfiles.py")


def getNoise(hist, verbose):
    hh = hist.Clone()
     
    fit = ROOT.TF1("pol0","pol0",0,2000) 
    hh.Fit("pol0","RQ") #line 
    p0 = fit.GetParameter(0)
    err = fit.GetParError(0)

    return p0, err

def getAvgAndRMS(data):
    avg = 0
    rms = 0
    for d in data:
        avg += d
    avg /= 1.0*len(data)

    for d in data:
        rms += (avg-d)**2
    rms /= 1.0*(len(data)-1)
    rms = rms**0.5

    return avg,rms



def getNoiseHist(hist, hybridName, verbose):
    tmp = ROOT.TH1F(hybridName+"Noise",hybridName+"Noise", 100, 7, 11)
    for i in xrange(1, hist.GetNbinsX()+1):
        tmp.Fill(hist.GetBinContent(i))
    return tmp

    

def getBadChannels(hist, noise, verbose):
    badchans = 0
    diff = 0
    diff_s = 0
    for i in xrange(1, hist.GetNbinsX()+1):
       bad = False
       chanNoise = hist.GetBinContent(i)
       if chanNoise > (noise + 1): bad = True
       if chanNoise < (noise - 1): bad = True
       
       if bad:
           badchans+=1
           if verbose: print "-- Channel %i : Noise %2.4f"%(i, chanNoise)
           if abs(noise - chanNoise) > diff:
               diff = abs(noise - chanNoise)
               diff_s = noise - chanNoise 
    return badchans, diff_s




parser = OptionParser()
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="If true, prints out bad channels and their noise")

basedir = "/eos/user/l/lhoryn/MagnetTest/"



measure = {
    "noise"  : {'label': "Noise [VCTH units]",                         'ymin': 7.5, 'ymax': 11.5},
    "noiseRMS"  : {'label': "RMS of Noise [VCTH units]",                         'ymin': 0.1, 'ymax': 0.5},
    "badChannels": {'label':  "# channels with noise +/- 1 from the mean", 'ymin': 0, 'ymax': 7},
}



graphs = {
    "barrel":{
        "Hybrid_0_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                },
        "Hybrid_0_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                },
        "Hybrid_1_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                },
        "Hybrid_1_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                }
    },
    "endcap":{
        "Hybrid_0_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[0]},
                },
        "Hybrid_0_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[1]},
                },
        "Hybrid_1_even": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[2]},
                },
        "Hybrid_1_odd": {
                  "noise"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "noiseRMS"  : {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                  "badChannels": {'x': array('d'), 'y': array('d'), 'err': array('d'), 'color': more_colors[3]},
                }
    }
            
}


fields = ["0", "1.5", "3"]
orients = ["barrel","endcap"]
hybrids = ["Hybrid_0_even", "Hybrid_1_even", "Hybrid_0_odd", "Hybrid_1_odd"]

info_vec = {}
for o in orients:
    info_vec[o] = {}
    for f in fields:
        info_vec[o][f] = {}
        for h in hybrids:
            # results per hybrid
            info_vec[o][f][h] = {}
            info_vec[o][f][h]["noise"]       = []
            info_vec[o][f][h]["badChannels"] = []
            info_vec[o][f][h]["noiseRMS"]    = []



(options, args) = parser.parse_args()

for key in all_files.keys():
    for f in all_files[key]:
        print f
        if options.verbose: print "Parsing ", f
        ff = ROOT.TFile(basedir+ f + "/Hybrid.root")
        for detector in ff.GetListOfKeys():
            d = detector.ReadObj()
            for board in d.GetListOfKeys():
                b = board.ReadObj()
                for opticalGroup in b.GetListOfKeys():
                    o = opticalGroup.ReadObj()
                    for hybrid in o.GetListOfKeys():
                        h = hybrid.ReadObj()
                        for chip in h.GetListOfKeys():
                            
                            ## make hybrid-level histograms
                            if "TH" in chip.GetClassName():
                                hname = chip.GetName()
                                
                                if "HybridStripNoiseEvenDistribution" in hname or "HybridStripNoiseOddDistribution" in hname:
                                    k = "even" if "Even" in hname else "odd"

                                    hist = chip.ReadObj()
                                    if options.verbose: print "-- now processing ", hybrid.GetName(), k 
                                    ### Measure noise per side of hybrid
                                    noise, error = getNoise(hist, options.verbose)
                                    noiseHist = getNoiseHist(hist,hybrid.GetName()+str(noise),  options.verbose)
                                    rms = noiseHist.GetRMS()
                                    chans, mxax = getBadChannels(hist, noise, options.verbose)
                                    
                                    orientation = "endcap"
                                    if "barrel" in key: orientation = "barrel"
                                    field = "0"
                                    if "3T" in key: field = "3"
                                    if "1.5T" in key: field = "1.5"
                                    hybName = hybrid.GetName() + "_" + k
                                    
                                    print orientation, field, hybName
                                                                         
                                    info_vec[orientation][field][hybName]["noise"].append(noise) 
                                    info_vec[orientation][field][hybName]["noiseRMS"].append(rms) 
                                    info_vec[orientation][field][hybName]["badChannels"].append(chans) 

                                    if options.verbose: print "-- Mean noise ", noise
                                    if options.verbose: print "-- Number of potentially bad channels", chans
                                    #if options.verbose: print "-- Largest noise difference", bigdiff
                                    if options.verbose: print 

    ff.Close()


for o in orients:
    for h in hybrids:
        for m in measure:
            for f in fields:
                graphs[o][h][m]['x'].append(float(f))
                avg, rms = getAvgAndRMS( info_vec[o][f][h][m] )
                graphs[o][h][m]['y'].append(avg)
                graphs[o][h][m]['err'].append(rms)

            print "printing ", o, h, m
            print graphs[o][h][m]['x']
            print graphs[o][h][m]['y']
            print graphs[o][h][m]['err']
            print
            if o == "barrel" and "Hybrid_1" in h and m == "badChannels":
                print "SOS"
                print  info_vec[o]["0"][h][m]





                 





c = ROOT.TCanvas()
print "now trying to plot"

xerr = array('d')
for x in xrange(3): xerr.append(0)

for meas in measure:
    c.Clear()
    leg = ROOT.TLegend(0.35,0.70,0.85,0.92)
    leg.SetFillStyle(0)
    first = True
    for label in orients:
        i=0
        for hybrid in hybrids:
            graphs[label][hybrid][meas]['graph'] = ROOT.TGraphErrors(3, graphs[label][hybrid][meas]['x'], graphs[label][hybrid][meas]['y'], xerr, graphs[label][hybrid][meas]['err'])
            graphs[label][hybrid][meas]['graph'].GetHistogram().SetMaximum(measure[meas]['ymax']) 
            graphs[label][hybrid][meas]['graph'].GetHistogram().SetMinimum(measure[meas]['ymin'])
            graphs[label][hybrid][meas]['graph'].SetTitle(';Magnetic Field [T];%s'%measure[meas]['label']) 
            graphs[label][hybrid][meas]['graph'].SetLineWidth(2) 
            graphs[label][hybrid][meas]['graph'].SetLineColor(more_colors[i]) 
            graphs[label][hybrid][meas]['graph'].SetMarkerColor(more_colors[i])
            i+=1

            if "endcap" in label:  
                graphs[label][hybrid][meas]['graph'].SetLineStyle(2) 
                graphs[label][hybrid][meas]['graph'].SetMarkerStyle(21) 
            
            leg.AddEntry(graphs[label][hybrid][meas]['graph'], hybrid + " " + label, 'pl')
             
            if first: 
                graphs[label][hybrid][meas]['graph'].Draw('pela')
                first = False
            else:
                graphs[label][hybrid][meas]['graph'].Draw('pelsame')

        
    leg.SetNColumns(2)
    leg.Draw('same')
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    c.SaveAs("plots/%sChange_summary.pdf"%(meas))


                             



