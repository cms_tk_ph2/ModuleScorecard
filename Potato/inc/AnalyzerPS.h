#ifndef AnalyzerPS_h__
#define AnalyzerPS_h__

#include "Analyzer.h"

// Analyzer
class AnalyzerPS : public Analyzer {

 public:
  
  AnalyzerPS();

  void analyze(std::string fileName) override;
  void printMetaData();

 private:
  
};

#endif
