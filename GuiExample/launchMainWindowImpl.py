import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import * 
from PyQt5.QtCore import * 
import datetime

from launchMainWindow import Ui_launchMW
from analyzeModuleMainWindowImpl import AnalyzeModuleMW
from scoreModuleMainWindowImpl import ScoreModuleMW


class LaunchMW (QMainWindow, Ui_launchMW):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)

    def closeEvent(self, event):
        dialog = QMessageBox.information(self, 'Done?', 'Are you sure you want to close?', buttons = QMessageBox.Yes|QMessageBox.No)
        if dialog == QMessageBox.Yes:
            sys.exit()
        else:
            event.ignore()

    # parse input file to get username & moduleID from metadata tree
    def getMetadata(self, inputFile):
        username = "lesya"
        moduleID = "100"
        return username, moduleID

    def launchAnalyzeModuleMW(self):
        username, moduleID = self.getMetadata(self.inputFileLE.text())
        self.analyzeModuleMW = AnalyzeModuleMW(username, moduleID)
        self.analyzeModuleMW.show()
    
    def launchScoreModuleMW(self):
        username, moduleID = self.getMetadata(self.inputFileLE.text())
        self.scoreModuleMW = ScoreModuleMW(username, moduleID)
        self.scoreModuleMW.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    theLaunchMW = LaunchMW()
    theLaunchMW.show()
    sys.exit(app.exec_())
