#include "Analyzer.h"
#include "Analyzer2S.h"
#include "Result.h"

#include <sstream>
#include <iostream>

#include <TFile.h>
#include <TDirectory.h>
#include <TKey.h>
#include <TObject.h>
#include <TH1.h>
#include <TGraphErrors.h>

///////////////////////////////////////////////////////////////////////
Analyzer2S::Analyzer2S()
{

//    std::vector<std::string> partParentNames = {"DATA_SET","PART"};
//    result->addNodeWithValue(partParentNames,"KIND_OF_PART","2S Module");
//    result->addNodeWithValue(partParentNames,"BARCODE","module01");

}

///////////////////////////////////////////////////////////////////////
void Analyzer2S::analyze(std::string fileName)
{

    // Define top level directory name for analysis
    fInputFileHandler.openFile(fileName);
    fInputFileHandler.makeResultTree(fResult);

    for(const auto& constHybrid: fResult.getHybrids())
    {
        Component& hybrid = fResult.getHybrid(constHybrid.first);
        getComponentNoise(hybrid);
        for(const auto& constChip: constHybrid.second.getComponents())
        {
            Component& chip = hybrid.getComponent(constChip.first);
        }
    }



    printMetaData();

    std::vector<std::string> top_level = {"DATA_SET","DATA"};


/*
    // Loop over contents of the directory
    TIter keys( fResult->directory->GetListOfKeys() );
    TKey *dkey;
    while ( dkey = (TKey*)keys() )
    {
        TObject *dobj = dkey->ReadObj();

        // Check if it is a directory
        if ( dobj->IsA()->InheritsFrom( TDirectory::Class() ) )
        {
            std::string keyName = dkey->GetName();

            // Check if it corresponds to a hybrid
            if(keyName.find("Hybrid") != std::string::npos)
            {
                fResult->addNode(top_level,keyName);

                std::vector<std::string> hybrid_level = top_level;
                hybrid_level.push_back(keyName);
                getPartNoise(hybrid_level, (TDirectory*)dobj, keyName);

                // Loop over contents of subdirectories
                TIter subkeys( ((TDirectory*) dobj)->GetListOfKeys() );
                TKey *dsubkey;
                while( dsubkey = (TKey*)subkeys() )
                {
                    TObject *dsubobj = dkey->ReadObj();

                    // Check if it is a directory
                    if ( dsubobj->IsA()->InheritsFrom( TDirectory::Class() ) )
                    {
                        std::string subKeyName = dsubkey->GetName();

                        // Check if it corresponds to a CBC chip
                        if(subKeyName.find("Chip") != std::string::npos)
                        {
                            fResult->addNode(hybrid_level,subKeyName);

                            std::vector<std::string> chip_level = hybrid_level;
                            chip_level.push_back(subKeyName);
                            getPartNoise    (chip_level, (TDirectory*)dsubobj, subKeyName);
                            getPartPedestal (chip_level, (TDirectory*)dsubobj, subKeyName);
                            getPartOffset   (chip_level, (TDirectory*)dsubobj, subKeyName);
                            getPartOccupancy(chip_level, (TDirectory*)dsubobj, subKeyName);
                            getPartVPlus    (chip_level, (TDirectory*)dsubobj, subKeyName);

                        } // End if contains "Chip"
                    } // End if is directory
                } // End loop over chips

            } // End if contains "Hybrid"
        } // End if is directory
    } // End loop over hybrids

    getPartIVScan(top_level);

    // Print output and write to XML file
    fResult->printData();
    fResult->writeToXml(outputXmlName);
*/
}

///////////////////////////////////////////////////////////////////////
void Analyzer2S::printMetaData(){
    std::cout << "This is 2S module" << std::endl;
}
