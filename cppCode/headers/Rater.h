#ifndef Rater_h__
#define Rater_h__

#include "headers/Result.h"
#include <string>

// Rater
class Rater{

 public:

  Rater();  
  void initializeFromFile(std::string);
  void initializeFromResult(Result* result);
  Result* fResult;
  
};

#endif
