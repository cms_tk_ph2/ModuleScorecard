#include "../headers/utils.h"
#include "../headers/Result.h"
#include "../headers/ResultHandler.h"
#include "../headers/InputFileHandler.h"
#include "../headers/DatabaseHandler.h"
#include "../headers/WebsiteHandler.h"
#include "../headers/cxxopts.hpp"

#include "TFile.h"
#include <iostream>
#include <fstream>
#include <sstream>

int main(int argc, char** argv){

    cxxopts::Options options("extract module features",  "extract module features");

    options.add_options()
        ("i,input"       , "Input file with Ph2_ACF directory structure",                                   cxxopts::value<std::string>())
        ("d,db"          , "Database file: either give me a csv to edit. If you don't I'll make a new one", cxxopts::value<std::string>()) 
        ("m,moduleID"    , "some value that represents the module. Stored as a string",                     cxxopts::value<std::string>()) 
        ("t,filetype"    , "indicates input file structure being used" ,                                    cxxopts::value<std::string>())
        ("h,help"        , "Print help");
    
    // module ID isn't needed anymore 
    //      we can pick it up from the rootfile or an associated config file from the calibration?
    // filetype isn't needed yet, but could be used specify 
    //      eg. burn-in box needs temperature timestamp matching
    
    auto opts = options.parse(argc, argv);

    if(opts.count("help") ){
        std::cout << options.help() << std::endl;
        return 1;
    }

    if (! opts.count("input") ){
        std::cout << options.help() << std::endl;
        std::cout << "You  must provide an input file!" << std::endl;
        return 1;
    }

    std::string dateTime = getDateTimeString();

    // Grab input file and prepare to grab some histograms from it 
    // for...as many files as needed? 
    // we decided to analyze one file at a time

    InputFileHandler inputFileHandler;
    inputFileHandler.setTopLevel("OpticalGroup"); // Define top level dirname for analysis
    inputFileHandler.openFile(opts["input"].as<std::string>());
    
    // Get a vector of Results to analyze
    std::cout << " " << std::endl;
    std::cout << "Getting results to analyze" << std::endl;
    std::cout << " " << std::endl;

    std::vector<Result*> results = {};
    inputFileHandler.getResults(results);


    // Setup a new ResultHandler 
    // for each module in the file
    // for each iteration...
    std::cout << " " << std::endl;
    std::cout << "Looping through results" << std::endl;
    std::cout << " " << std::endl;

    for (auto result : results)
    {

        // Do analysis on module/iteration
        ResultHandler *analysis = new ResultHandler(result);
        analysis->doAnalysis();
        analysis->updateResult(result);
        analysis->printResults();
 
        
        // Take updated result and save to database
        DatabaseHandler *hybridResults = new DatabaseHandler(result);

        // If we specified smodule name or database   
        if (opts.count("moduleID")) hybridResults->setModuleID(opts["moduleID"].as<std::string>());
        if( opts.count("db")) {
            hybridResults->setOutputPath(opts["db"].as<std::string>());
        }
        else {
            hybridResults->setOutputPath("results_"+dateTime+"_module_"+result->moduleName+".csv");
        }

        // Save to CSV
        hybridResults->dumpToCSV();

    }

}
// What do we want in output files 
// Multiple iterations per module

